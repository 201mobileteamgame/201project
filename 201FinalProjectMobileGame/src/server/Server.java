package server;

import java.net.ServerSocket;
import java.net.Socket;
import java.util.Vector;

import game.GameManager;
import game.Player;

public class Server {
	private ServerSocket ss = null;
	private Socket s;
	private ServerListener serverListener;
	private int portNumber = -1;
	
	private GameManager gm;
	
	public Server(ServerSocket ss) {
		this.ss = ss;
		//empty constructor for now
		//to be modified later
		listenForConnections();
	}
	
	//kicks of the server thread
	public void listenForConnections() {
		serverListener = new ServerListener(ss,this);
		serverListener.start();
	}
	
	//generates the ServerSocket with a port value
/*	public ServerSocket createPort() {
		try {
			ServerSocket tempss = new ServerSocket(portNumber);
			ss = tempss;
		} catch(IOException ioe) {
			System.out.println("ioe in createPort(): " + ioe.getMessage());
		}
		return ss;
	}*/
	
	public void setPort(int portNum) {
		portNumber = portNum;
	}
	
	public void setUpGameManager(Vector<Player> players) {
		gm = new GameManager();
		gm.setUp(players, this);
	}
	
	public GameManager getGameManager() {
		return gm;
	}
	
	public Socket getSocket() {
		return s;
	}
	
	public ServerSocket getSocketServer() {
		return ss;
	}
	
	public void sendMessage(String message) {
		if (serverListener != null) {
			serverListener.sendMessageToAllClients(message);
		}
	}
	
	//Store messages for possible users that connect later
	public void putMessage(String message) {
		if (serverListener != null) {
			serverListener.putMessage(message);
		}
	}
}
