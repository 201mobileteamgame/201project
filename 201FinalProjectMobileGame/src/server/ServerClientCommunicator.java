package server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;
import java.util.Vector;

public class ServerClientCommunicator extends Thread {

	private Socket socket;
	private ServerListener serverListener;
	private BufferedReader br;
	private PrintWriter pw;
	
	
	public ServerClientCommunicator(Socket socket, ServerListener serverListener) throws IOException {
		this.socket = socket;
		this.serverListener = serverListener;
		this.br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		this.pw = new PrintWriter(socket.getOutputStream());
	}
	
	public Socket getSocket() {
		return socket;
	}
	
	public void sendMessage(String message) {
		pw.println(message);
		pw.flush();
	}
	
	public void run() {
		Scanner sc = null;
		try {
			while(true) {
				System.out.println("WAITING FOR A NEW MESSAGE!!!!>...............................");
				String line = br.readLine();
				if(line.equals(null)) break;
				System.out.println("scc in message = "+line);
				sc = new Scanner(line);
				String taskType = sc.next();
				switch(taskType) {
				//Do tasks
				case "RequestGuestNumber" :
					sendMessage("Guest: "+serverListener.getGuestNumber());
					break;
				case "NewPlayer:" :
					String playerName = sc.next();
					String newPlayerNameMessage = "AddNewPlayer: "+playerName;
					serverListener.addStringPlayer(playerName);
					serverListener.sendMessageToAllClients(newPlayerNameMessage);
					serverListener.putMessage(newPlayerNameMessage);
					break;
				case "GameStart" :
					if(!serverListener.getAlreadySetUp()) {
						serverListener.enterTeamPhase();
						serverListener.setSetUp();
					}
//					serverListener.createPlayerVector();
//					serverListener.close();
//					serverListener.setUpGameManager();
//					serverListener.sendMessageToAllClients("EnterTeamPanel");
					break;
				case "LeaderTeam:" :
					serverListener.clearTeamVector();
					for(int i=0;i<4;i++) {
						String newTeamMember = sc.next();
						serverListener.addToTeamVector(newTeamMember);
					}
			//		serverListener.sendMessageToAllClients(teamString);
					break;
				case "Vote:" :
					serverListener.addVote(sc.next());
					break;
				case "ButtonClicked:" :
					int buttonClicked = sc.nextInt();
					String clickName = sc.next();
					serverListener.receiveGameDoneTask(buttonClicked, clickName);
					break;
//				case "TaskCompleted" :
//					serverListener.sendMessageToAllClients("TaskCompleted");
//					break;
				case "ConfirmQuit" :
					serverListener.addQuit();
					break;
				}
			}
		} catch(IOException ioe) {
			System.out.println("ioe in ServerClientCommunicator.run().catch: "+ioe.getMessage());
		} catch(NullPointerException npe) {
			System.out.println("npe in ServerClientCommunicator.run().catch: "+npe.getMessage());
		}
		finally {
			if(sc!=null) sc.close();
			serverListener.removeServerClientCommunicator(this);
			try {
				socket.close();
			} catch(IOException ioe) {
				System.out.println("ioe in ServerClientCommunicator.run().finally: "+ioe.getMessage());
			}
		}
	}
}
