package server;

import java.io.IOException;
import java.net.BindException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Vector;

import game.Player;

public class ServerListener extends Thread {
	private ServerSocket ss;
	private Vector<ServerClientCommunicator> sccVector;
	private int numPlayers = 0;
	private Vector<String> playerStringVector;
	private Vector<Player> players;
	
	private Vector<String> messageVect = new Vector<String>();
	private Server server;
//	private boolean gameStarted = false;
	private int guestNumber = 0;
	
	private boolean alreadySetUp = false;
	private int quitNum = 0;
	
	private Vector<String> teamVector;
	
	//Data tracking
	private int vote = 0;
	
	public ServerListener(ServerSocket ss, Server server) {
		this.ss = ss;
		this.server = server;
		sccVector = new Vector<ServerClientCommunicator>();
		players = new Vector<Player>();
		teamVector = new Vector<String>();
		playerStringVector = new Vector<String>();
	}
	
	public void setSetUp() {
		alreadySetUp = true;
	}
	
	public boolean getAlreadySetUp() {
		return alreadySetUp;
	}
	
	//Store messages for clients that connect after some data have been processed
	public void putMessage(String message) {
		messageVect.addElement(message);
	}

	public void sendMessageToAllClients(String message) {
		System.out.println("Send to all: "+message);
		for(ServerClientCommunicator scc : sccVector) {
			scc.sendMessage(message);
		}
	}
	
	public void addStringPlayer(String name) {
		playerStringVector.add(name);
	}
	
	public void createPlayerVector() {
		if(!players.isEmpty()) {
			players.removeAllElements();
		}
		for(String playerName : playerStringVector) {
			System.out.println("CREATING PLAYER: "+playerName);
			Player player = null;
			if(playerName.contains("Guest")) {
				player = new Player(false);
			} else {
				player = new Player(true);
			}
			player.setPlayerName(playerName);
			players.add(player);
		}
	}
	
	public void addQuit() {
		quitNum+=1;
		if(quitNum==sccVector.size()) {
			quitGame();
		}
	}
	
	private void quitGame() {
		System.exit(0);
	}
	
	public void close() {
		try {
			if(ss!=null) {
				ss.close();
			}
		} catch(IOException ioe) {
			System.out.println("ioe in closing ss: "+ioe.getMessage());
		}
	}
	
	public void enterTeamPhase() {
		if(alreadySetUp) return;
		createPlayerVector();
		close();
		setUpGameManager();
		sendMessageToAllClients("EnterTeamPanel");
	}
	
	public void removeServerClientCommunicator(ServerClientCommunicator scc) {
		System.out.println("Client disconnected: " + scc.getSocket().getInetAddress());
		sccVector.remove(scc);
		quitGame();
	}
	
	public int getGuestNumber() {
		guestNumber+=1;
		return guestNumber;
	}
	
	public void setUpGameManager() {
		server.setUpGameManager(players);
	}
	
	public void addVote(String vote) {
		if(vote.equals("Approve")) {
			server.getGameManager().addVote(true);
		} else if(vote.equals("Reject")){
			server.getGameManager().addVote(false);
		}
	}
	
	public void clearTeamVector() {
		teamVector.removeAllElements();
	}
	
	public void addToTeamVector(String name) {
		teamVector.add(name);
		if(teamVector.size()==4) {
			server.getGameManager().setSquad(teamVector);
		}
	}
	
	public Vector<String> getTeamVector() {
		return teamVector;
	}
	
	public void receiveGameDoneTask(int button, String playerName) {
		server.getGameManager().getCurrentMission().receiveDoneTask(button, playerName);
	}
	
	public void run() {
		System.out.println("Server thread start");
		try {
			//TODO loop for specified player number
			while(sccVector.size()<=10) {
				//TODO fix for testing
				if(sccVector.size()>=5) {
					sendMessageToAllClients("EnableStartGame");
				}
				System.out.println("waiting...");
				Socket s = ss.accept();
				System.out.println(s.getInetAddress());
				System.out.println(sccVector.size()+1);
				try {
					// this line can throw an IOException
					// if it does, we won't start the thread
					ServerClientCommunicator scc = new ServerClientCommunicator(s, this);
					scc.start();
					
					//after client connects send messages processed information
					for(String message : messageVect) {
						scc.sendMessage(message);
					}
					sccVector.add(scc);
					
				} catch (IOException ioe) {
					System.out.println(ioe.getMessage());
				}
			}
		} catch(BindException be) {
			System.out.println(be.getMessage());
		} catch (IOException ioe) {
			System.out.println(ioe.getMessage());
		} finally {
			if (ss != null) {
				try {
					ss.close();
				} catch (IOException ioe) {
					System.out.println(ioe.getMessage());
				}
			}
		}
	}
}
