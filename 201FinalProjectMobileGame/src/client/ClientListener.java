package client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;
import java.util.Vector;

import javax.swing.JOptionPane;

import gameGUI.GameWindow;

public class ClientListener extends Thread {
	private Socket s;
	private BufferedReader br;
	private PrintWriter pw;
	private boolean socketReady;
	private boolean clientConnected;
	
	private Client client;
	
	private GameWindow gw;
	
	public ClientListener(Socket s, GameWindow gw, Client c) {
		this.s = s;
		this.gw = gw;
		client = c;
		clientConnected = false;
		socketReady = initializeVariables();
		if(socketReady) {
			start();
		}
	}
	
	public boolean initializeVariables() {
		try{
			br = new BufferedReader(new InputStreamReader(s.getInputStream()));
			pw = new PrintWriter(s.getOutputStream());
		} catch(IOException ioe) {
			System.out.println("ioe in ClientListener constructor: "+ioe.getMessage());
			return false;
		}
		return true;
	}
	
	public void sendMessage(String message) {
		/*TODO : Add method to send messages to server, needs to include
		 * Chat messages
		 * Squad selection by leader
		 * Votes to approve/reject squad
		 * All mission minigame information (button presses)
		 * etc.
		 */
		pw.println(message);
		pw.flush();
	}
	
	public void run() {
		/*TODO : Add method to recieve messages from server, needs to include
		 * Chat messages
		 * Other player info for setup
		 * Game state info (squad selections, voting results)
		 * All mission minigame information (task assignment)
		 * etc.
		 */
		
		//Scanner for scanning incoming messages
		Scanner sc = null;
		try {
			String line =  null;
			while(true) {
				line = br.readLine();
				if(line==null) {
					System.out.println("LINE IS NULL");
					System.out.println("!!!!!!!!!!!!!!!!!!!!!!");
					System.out.println("!!!!!!!!!!!!!!!!!!!!!!");
					System.out.println("!!!!!!!!!!!!!!!!!!!!!!");
					System.out.println("!!!!!!!!!!!!!!!!!!!!!!");
					break;
				}
				clientConnected = true;
				System.out.println("clientListener in message = "+line);
				sc = new Scanner(line);
				String msgType = sc.next();
				switch(msgType) {
				//TODO Manage different messages by scanning the rest of the message
				case "Guest:" :
					int guestID = sc.nextInt();
					client.getPlayer().setPlayerName("Guest"+guestID);
					client.sendMessage("NewPlayer: "+client.getPlayer().getPlayerName());
					break;
				case "AddNewPlayer:" :
					String newPlayerName = sc.next();
					client.addPlayer(newPlayerName);
					gw.getWaitPanel().displayPlayers();
					break;
				case "EnableStartGame" :
					gw.getWaitPanel().enableStart();
					break;
				case "Traitors:" :
					Vector<String> traitors = new Vector<String>();
					boolean isTraitor = false;
					while(sc.hasNext()) {
						String traitorName = sc.next();
						traitors.add(traitorName);
						gw.getStatsPanel().addSpies(traitorName);
						if(client.getPlayer().getPlayerName().equals(traitorName)) {
							isTraitor = true;
							client.getPlayer().setTraitor(true);
							gw.setTraitor(true);
						}
					}
					if(!isTraitor) {
						client.getPlayer().setTraitor(false);
						gw.setTraitor(false);
					}
					System.out.println("Player's role is: "+client.getPlayer().getRole());
					gw.getRolePanel().setTraitors(traitors);
					break;
				case "StartSquad:":
					String newLeaderName = sc.next();
					gw.getTeamPanel().setLeaderName(newLeaderName);
					break;
				case "EnterTeamPanel" :
					gw.getWaitPanel().enterTeamPanel();
					break;
				case "StartVote:" :
					gw.getTeamPanel().clearTeam();
					for(int i=0;i<4;i++) {
						String newTeamMember = sc.next();
						gw.getTeamPanel().setProposedTeam(newTeamMember);
					}
					gw.getTeamPanel().displayProposedTeam();
					gw.getTeamPanel().enableAllButtons(true);
					break;
				case "StopVoting" :
					gw.getTeamPanel().enableAllButtons(false);
					break;
				case "StartMission" :
					gw.getTeamPanel().startMission();
					break;
				case "TotalNumTasks:" :
					gw.setTotalNumTasks(sc.nextInt());
					break;
				case "NewTask:" :
					String taskPlayerName = sc.next();
					String taskString = sc.nextLine();
					taskString.trim();
					gw.sendNewTask(taskPlayerName,taskString);
					break;
				case "Freeze" :
					gw.freezeAllMissions();
					break;
				case "Misclick:" :
					gw.getSpectator().showMisclick(sc.next());
					break;
				case "StopMission" :
					gw.quitAllMissions();
					break;
				case "StartResult:" :
					gw.getTeamPanel().clearLeader();
					gw.getTeamPanel().clearTeam();
					break;
				case "Completed" :
					//TODO update progress bar
					gw.updateTasksCompleted();
					break;
				case "TraitorsWinMission" :
					gw.getTeamPanel().getTimerPanel().getGameScore().setTraitorsWin();
					break;
				case "PatriotsWinMission" :
					gw.getTeamPanel().getTimerPanel().getGameScore().setPatriotsWin();
					break;
				case "TraitorVictory:" :
					gw.getStatsPanel().setWinningTeam("Traitors");
					gw.getStatsPanel().setPlayerWin(client.getPlayer().getRole());
					sc.next();
					sc.next();
					sc.next();
					int numRounds = sc.nextInt()+1;
					gw.getStatsPanel().setNumRounds(numRounds);
					gw.callGameEnd();
					break;
				case "PatriotVictory:" :
					gw.getStatsPanel().setWinningTeam("Patriots");
					gw.getStatsPanel().setPlayerWin(client.getPlayer().getRole());
					sc.next();
					sc.next();
					sc.next();
					int numberRounds = sc.nextInt()+1;
					gw.getStatsPanel().setNumRounds(numberRounds);
					gw.callGameEnd();
					break;
				}
				
			}
		} catch(Exception e) {
			System.out.println("ioe in JoinClient.run(): "+e.getMessage());
			if(sc!=null) sc.close();
			try {
				if(s!=null) {
					s.close();
				}
			}catch(IOException ioe) {
				System.out.println("ioe in ClientListener.run().catch(e): "+e.getMessage());
			}
		} finally {
			if(clientConnected) {
			/*	JOptionPane.showMessageDialog(
						null, 
						"Host left the game", 
						"Sorry!", 
						JOptionPane.NO_OPTION
					);*/
				System.exit(0);
				//TODO Return to main menu if host quitted the game
	/*			JButton exit = new JButton("");
				exit.addActionListener(cp.getQuitAction());
				exit.doClick();*/
			}
		}
	}
}
