package client;

import java.net.Socket;
import java.util.Vector;

import game.Player;
import gameGUI.GameWindow;

public class Client {
	private Socket s;
	private ClientListener cl;
	
	private Player player = null;
	private Vector<String> players;
	
	private GameWindow gw;

	public Client(Socket s, GameWindow gw) {
		this.s = s;
		this.gw = gw;
		gw.setClient(this);
		players = new Vector<String>();
		startListening();
		System.out.println("Started listening");
		if(!gw.isLoggedIn()) {
			System.out.println("Player is not logged in");
			player = new Player(false);
			cl.sendMessage("RequestGuestNumber");
		}
		else {
			System.out.println("Player is logged in");
		}
	}
	
	public void sendMessage(String message) {
		cl.sendMessage(message);
	}
	
	private void startListening() {
		cl = new ClientListener(s,gw,this);
	}
	
	public void setPlayer(Player inPlayer){
		System.out.println("reach set player");
		player = inPlayer;
		cl.sendMessage("NewPlayer: "+player.getPlayerName());
	}
	
	public Player getPlayer() {
		return player;
	}
	
	public void addPlayer(String playerName) {
		players.addElement(playerName);
	}
	
	public void removePlayer(String playerName) {
		players.removeElement(playerName);
	}
	
	public Vector<String> getPlayerVector() {
		return players;
	}
	
}
