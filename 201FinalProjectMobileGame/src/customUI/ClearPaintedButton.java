package customUI;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Image;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;

public class ClearPaintedButton extends JButton {
	private static final long serialVersionUID = 1L;

	private JLabel buttonTextLabel;
	private Color buttonColor = null;
	private Font buttonFont = null;
	private Image bgImage = null;
	
	public ClearPaintedButton(Font font, Color color, Image img) {
		buttonColor = color;
		bgImage = img;
		setBackground(new Color(0,0,0,0));
		buttonFont = font;
		this.setBorder(BorderFactory.createEmptyBorder());
		
		setLayout(new GridLayout(1,1));
		
		buttonTextLabel = new JLabel();
		buttonTextLabel.setForeground(buttonColor);
		buttonTextLabel.setFont(buttonFont);
		buttonTextLabel.setHorizontalAlignment(JButton.CENTER);	
		buttonTextLabel.setVerticalAlignment(JButton.CENTER);
		
		add(buttonTextLabel);
		
		setOpaque(false);
		setContentAreaFilled(false);
		setBorderPainted(false);
	}
	
	public void setButtonText(String text) {
		buttonTextLabel.setText(text);
	}
	
	public void setEnabled(boolean b){
        if (b){
            setForeground(Color.BLACK);
            buttonTextLabel.setForeground(buttonColor);
        } else {
        	setForeground(Color.LIGHT_GRAY);
            buttonTextLabel.setForeground(Color.LIGHT_GRAY);
        }
        super.setEnabled(b);
    }
	

	@Override
	protected void paintComponent(Graphics g) {
		g.drawImage(bgImage, 0, 0, getWidth(), getHeight(), null);
	}
	
}
