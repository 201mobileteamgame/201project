package customUI;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JLabel;

public class ClearButton extends JButton {
	private static final long serialVersionUID = 1L;

	private JLabel buttonTextLabel;
	private Color buttonColor = null;
	private Font buttonFont = null;
	
	public ClearButton(String buttonText, Font font, Color color) {
		buttonColor = color;
		setBackground(new Color(0,0,0,0));
		buttonFont = font;
		
		setLayout(new GridLayout(1,1));
		
		setOpaque(false);
		setContentAreaFilled(false);
		setBorderPainted(false);
		
		buttonTextLabel = new JLabel(buttonText);
		buttonTextLabel.setForeground(color);
		buttonTextLabel.setFont(font);
		buttonTextLabel.setHorizontalAlignment(JButton.CENTER);	
		buttonTextLabel.setVerticalAlignment(JButton.CENTER);
		
		add(buttonTextLabel);
	}
	
	public ClearButton(Font font, Color color) {
		buttonColor = color;
		setBackground(new Color(0,0,0,0));
		buttonFont = font;
		
		setLayout(new GridLayout(1,1));
		
		buttonTextLabel = new JLabel();
		buttonTextLabel.setForeground(buttonColor);
		buttonTextLabel.setFont(buttonFont);
		buttonTextLabel.setHorizontalAlignment(JButton.CENTER);	
		buttonTextLabel.setVerticalAlignment(JButton.CENTER);
		
		add(buttonTextLabel);
		
		setOpaque(false);
		setContentAreaFilled(false);
		setBorderPainted(false);
	}
	
	public void setButtonText(String text) {
		buttonTextLabel.setText(text);
	}
	
	public void setEnabled(boolean b){
        if (b){
            setForeground(Color.BLACK);
            buttonTextLabel.setForeground(buttonColor);
        } else {
        	setForeground(Color.LIGHT_GRAY);
            buttonTextLabel.setForeground(Color.LIGHT_GRAY);
        }
        super.setEnabled(b);
    }
	
}
