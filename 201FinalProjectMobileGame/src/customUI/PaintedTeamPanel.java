package customUI;
 
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.JPanel;

import gameGUI.Constants;
import library.ImageLibrary;

public class PaintedTeamPanel extends JPanel{
	private static final long serialVersionUID = 1L;
 
	private Image mainImage;
	protected boolean mDrawBack = false;
	protected Color mBackColor = Color.WHITE;
	
	protected JPanel topPanel;
	protected JPanel bottomPanel;
	
	public PaintedTeamPanel() {
		setLayout(new BorderLayout());
		setBackground(new Color(0,0,0,0));
		mainImage = ImageLibrary.getImage("resources/images/mainBGImage.png");
		
		topPanel = new JPanel();
		topPanel.setPreferredSize(Constants.topEmptyPanelDimension);
		topPanel.setOpaque(false);
		bottomPanel = new JPanel();
		bottomPanel.setPreferredSize(Constants.bottomEmptyPanelDimension);
		bottomPanel.setOpaque(false);
		JPanel westEmptyPanel = new JPanel();
		westEmptyPanel.setPreferredSize(Constants.westEmptyPanelDimension);
		westEmptyPanel.setOpaque(false);
		JPanel eastEmptyPanel = new JPanel();
		eastEmptyPanel.setPreferredSize(Constants.eastEmptyPanelDimension);
		eastEmptyPanel.setOpaque(false);
		
		add(topPanel,BorderLayout.NORTH);
		add(bottomPanel,BorderLayout.SOUTH);
		add(westEmptyPanel,BorderLayout.WEST);
		add(eastEmptyPanel,BorderLayout.EAST);
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		if(topPanel.isOpaque()) {
			g.drawImage(mainImage, 0, topPanel.getPreferredSize().height, getWidth(), getHeight()-topPanel.getPreferredSize().height, null);
		}
		else {
			g.drawImage(mainImage, 0, 0, getWidth(), getHeight(), null);
		}
 	}
	
}
