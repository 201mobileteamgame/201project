package customUI;

import java.awt.Image;

import javax.swing.ImageIcon;

public class ResizeIcon {

	public static ImageIcon rs(ImageIcon icon, int height, int width) {
		Image image = icon.getImage(); 
		Image newimg = image.getScaledInstance(width, height,  java.awt.Image.SCALE_SMOOTH);
		ImageIcon returnIcon = new ImageIcon(newimg);
		return returnIcon;
	}
}
