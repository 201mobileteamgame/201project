package customUI;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Image;

import javax.swing.JButton;
import javax.swing.JLabel;

import library.FontLibrary;

public class PaintedButton extends JButton {
	private static final long serialVersionUID = 1L;
	
	private Image bgImage = null;
	private JLabel buttonTextLabel;

	public PaintedButton(String buttonText, Font font, Image image) {
		bgImage = image;
		setBackground(new Color(0,0,0,0));
		this.setOpaque(false);
		
		setLayout(new GridLayout(1,1));
		
		buttonTextLabel = new JLabel(buttonText);
		buttonTextLabel.setFont(font);
		buttonTextLabel.setHorizontalAlignment(JButton.CENTER);	
		buttonTextLabel.setVerticalAlignment(JButton.CENTER);
		
		add(buttonTextLabel);
	}
	
	
	@Override
	public void setEnabled(boolean b){
        if (b){
            setForeground(Color.BLACK);
            buttonTextLabel.setForeground(Color.BLACK);
        } else {
            setForeground(Color.LIGHT_GRAY);
            buttonTextLabel.setForeground(Color.LIGHT_GRAY);
        }
        super.setEnabled(b);
    }
	
	@Override
	protected void paintComponent(Graphics g) {
		g.drawImage(bgImage, 0, 0, getWidth(), getHeight(), null);
	}
	
	@Override
	protected void paintBorder(Graphics g) {
		//paint no border
	}
	
}
