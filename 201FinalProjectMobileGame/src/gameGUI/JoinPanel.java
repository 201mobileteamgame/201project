package gameGUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.Socket;

import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import client.Client;
import customUI.PaintedButton;
import customUI.PaintedPanel;
import gameGUI.GameWindow.SwitchPageAction;
import library.FontLibrary;
import library.ImageLibrary;

public class JoinPanel extends PaintedPanel {
private static final long serialVersionUID = 1L;
	private JPanel portPanel;
	
	private PaintedButton enterButton;

	private String enterPortString = "Please Enter Port Number:";
	private JLabel enterPortLabel;
	private JTextField portTF;
	
	private String enterIPString = "Please Enter IP Address:";
	private JLabel enterIPLabel;
	private JTextField ipTF;
	
	private JPanel errorPanel;
	private JLabel portErrorLabel;
	
	private Socket s;
	private Client client;
	
	private GameWindow gw;

	private SwitchPageAction mWaitAction;
	
	private SwitchPageAction mRecordsAction, mLoginAction;
	
	public JoinPanel(SwitchPageAction spa, GameWindow gw, SwitchPageAction inRecordsAction, SwitchPageAction inLoginAction) {
		mWaitAction = spa;
		this.gw = gw;
		mRecordsAction = inRecordsAction;
		mLoginAction = inLoginAction;
		initializeComponents();
		createGUI();
		addEvents();
	}

	private void initializeComponents() {
		portPanel = new JPanel();
		enterPortLabel = new JLabel(enterPortString);
		portTF = new JTextField();
		enterButton = new PaintedButton("Enter",
				FontLibrary.getFont("resources/fonts/digital.ttf", Font.BOLD, 50),
				ImageLibrary.getImage("resources/images/buttons/redButton.png")
				);
		
		enterIPLabel = new JLabel(enterIPString);
		ipTF = new JTextField();
		errorPanel = new JPanel();
		portErrorLabel = new JLabel();
		
		topPanel = new PlayerInfoOptionsPanel(mRecordsAction, mLoginAction);
		topPanel.setOpaque(false);
		add(topPanel, BorderLayout.NORTH);
		
		enterPortLabel.setFont(FontLibrary.getFont("resources/fonts/digital.ttf", Font.BOLD, 30));
		portTF.setFont(FontLibrary.getFont("resources/fonts/digital.ttf", Font.BOLD, 60));
		portTF.setHorizontalAlignment(JTextField.CENTER);
		
		enterIPLabel.setFont(FontLibrary.getFont("resources/fonts/digital.ttf", Font.BOLD, 30));
		ipTF.setFont(FontLibrary.getFont("resources/fonts/digital.ttf", Font.BOLD, 40));
		ipTF.setHorizontalAlignment(JTextField.CENTER);
	}

	private void createGUI() {
		portPanel.setLayout(new GridLayout(7,1));
		portPanel.setOpaque(false);
		
		errorPanel.setOpaque(false);
		errorPanel.setLayout(new GridLayout(1,1));
		errorPanel.add(portErrorLabel);
		
		portErrorLabel.setOpaque(false);
		portErrorLabel.setHorizontalAlignment(JLabel.CENTER);
		portErrorLabel.setFont(FontLibrary.getFont("resources/fonts/digital.ttf", Font.BOLD, 30));
		portErrorLabel.setForeground(Color.RED);
		
		portPanel.add(enterIPLabel);
		portPanel.add(ipTF);
		portPanel.add(enterPortLabel);
		portPanel.add(portTF);
		portPanel.add(Box.createGlue());
		portPanel.add(enterButton);
		portPanel.add(errorPanel);

		add(portPanel,BorderLayout.CENTER);
	}
	
	private void addEvents() {
		enterButton.addActionListener(new ConnectListener());
	}
	
	public Client getClient() {
		return client;
	}
	
	class ConnectListener implements ActionListener {
		public void actionPerformed(ActionEvent ae) {
			String portStr = portTF.getText();
			int portInt = -1;
			try {
				portInt = Integer.parseInt(portStr);
			} catch (Exception e) {
				portErrorLabel.setText(e.getMessage());
				return;
			}
			if (portInt > 0 && portInt < 65535) {
				// try to connect
				String hostnameStr = ipTF.getText();
				if(hostnameStr.isEmpty()) {
					portErrorLabel.setText("Empty Host Name");
					return;
				}
				try {
					s = new Socket(hostnameStr, portInt);
					client = new Client(s,gw);
					if(gw.isLoggedIn()) {
						client.setPlayer(gw.getLoggedInPlayer());
					}
					mWaitAction.actionPerformed(ae);
					return;
				} catch (IOException ioe) {
					portErrorLabel.setText(ioe.getMessage());
					System.out.println("ioe in JoinPanel.Connectlistener.actionPerformed(): "+ioe.getMessage());
					return;
				}
			}
			else { // port value out of range
				portErrorLabel.setText("Port value out of Range");
				return;
			}
		}
	}
	
}
