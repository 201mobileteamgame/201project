package gameGUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;

import customUI.ClearPaintedButton;
import customUI.PaintedPanel;
import gameGUI.GameWindow.SwitchPageAction;
import library.ImageLibrary;
import sound.GameAudioNoLoop;

public class MiniGame2 extends PaintedPanel {
	private static final long serialVersionUID = 1L;
	
	Font font;
	private ClearPaintedButton[] optionButtons;
	private final int numOptions = 4;
	Border emptyBorder;
	private int totalMissions = 0;
	
	JLabel instructionText;
	JPanel mainPanel;
	InstructionScreen instructionPanel;
	CountDownTimer timer = null;
	TimerGUI teamTimer = null;
	
	private JPanel topEmptyPanel;
	private JPanel bottomEmptyPanel;
	private JPanel westEmptyPanel;
	private JPanel eastEmptyPanel;
	
	SwitchPageAction mTeamAction;
	
	private GameWindow gw;
	
	private MiniGameProgressPanel progressPanel;
	
	public MiniGame2(SwitchPageAction spa, GameWindow gw) {
		this.gw = gw;
		mTeamAction = spa;
		emptyBorder = BorderFactory.createEmptyBorder();
		font = Constants.miniGameScreenFont;
		
		teamTimer = new TimerGUI();
		
		progressPanel = new MiniGameProgressPanel();
		
		topEmptyPanel = new JPanel();
		topEmptyPanel.setPreferredSize(Constants.topEmptyPanelDimension);
		topEmptyPanel.setLayout(new GridLayout(1,3));
		topEmptyPanel.add(teamTimer);
		topEmptyPanel.setBorder(new EmptyBorder(10,15,10,15));
		topEmptyPanel.add(new JLabel());
		topEmptyPanel.add(new JLabel());
		topPanel.setOpaque(false);
		topEmptyPanel.setOpaque(false);
		bottomEmptyPanel = new JPanel();
		bottomEmptyPanel.setLayout(new GridLayout(2,1));
		bottomEmptyPanel.setPreferredSize(Constants.bottomEmptyPanelDimension);
		bottomEmptyPanel.setOpaque(false);
		bottomEmptyPanel.add(new JLabel());
		bottomEmptyPanel.add(progressPanel);
		westEmptyPanel = new JPanel();
		westEmptyPanel.setPreferredSize(Constants.westEmptyPanelDimension);
		westEmptyPanel.setOpaque(false);
		eastEmptyPanel = new JPanel();
		eastEmptyPanel.setPreferredSize(Constants.eastEmptyPanelDimension);
		eastEmptyPanel.setOpaque(false);
		
		mainPanel = new JPanel();
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.PAGE_AXIS));
		mainPanel.setBorder(emptyBorder);
		mainPanel.setBackground(new Color(0,0,0,0));
		
		instructionPanel = new InstructionScreen();
		instructionPanel.setBackground(new Color(0,0,0,0));
		instructionPanel.setLayout(new BorderLayout());
		
		instructionText = new JLabel();
		instructionText.setForeground(Color.WHITE);
		instructionText.setFont(font);
		instructionText.setHorizontalAlignment(SwingConstants.CENTER);
		
		instructionPanel.add(instructionText);
		mainPanel.add(instructionPanel);
		
		//The middle of the panel, the answers buttons
		JPanel centerPanel = new JPanel(new GridLayout(2,2,5,5));
		centerPanel.setPreferredSize(new Dimension(540,250));
//		centerPanel.setBackground(new Color(0,0,0,0));
		centerPanel.setOpaque(false);
//		centerPanel.setBorder(new EmptyBorder(50,0,0,0));
		optionButtons = new ClearPaintedButton[numOptions];
		font = Constants.miniGameButtonsFont;
			for(int i = 0; i < numOptions; ++i) {
				optionButtons[i] = new ClearPaintedButton(font,Color.GREEN,Constants.miniButtonImage);
//				optionButtons[i].setIcon(Constants.redButtonIcon);
//				optionButtons[i].setPressedIcon(Constants.redButtonPressedIcon);
				
				if(i==0) optionButtons[i].setButtonText("Set Off Bomb");
				else if(i==1) optionButtons[i].setButtonText("Get Kidnapped");
				else if(i==2) optionButtons[i].setButtonText("Chase Target");
				else if(i==3) optionButtons[i].setButtonText("Brood Moodily");
				
				optionButtons[i].setHorizontalTextPosition(JButton.CENTER);
				optionButtons[i].setVerticalTextPosition(JButton.CENTER);
				
				final int buttonSelection = i+4;
				optionButtons[i].addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent ae) {
						GameAudioNoLoop ganl = new GameAudioNoLoop("resources/audio/gun-gunshot-01.wav");
						gw.getClient().sendMessage("ButtonClicked: "+buttonSelection+" "+gw.getClient().getPlayer().getPlayerName());
					}
				});
//				optionButtons[i].setFont(font);
				optionButtons[i].setBorder(emptyBorder);
				optionButtons[i].setEnabled(false);
				centerPanel.add(optionButtons[i]);
			}
		mainPanel.add(centerPanel);
		mainPanel.setOpaque(false);
		add(mainPanel, BorderLayout.CENTER);
		add(topEmptyPanel,BorderLayout.NORTH);
		add(bottomEmptyPanel,BorderLayout.SOUTH);
		add(westEmptyPanel,BorderLayout.WEST);
		add(eastEmptyPanel,BorderLayout.EAST);
	}
	
	public void setTotalMission(int num) {
		totalMissions = num;
		progressPanel.setTotal(totalMissions);
	}
	
	public void updateTasks(){
		progressPanel.updateCompleted();
	}
	
	public void startGame() {
		setInstruction("");
		for(JButton button : optionButtons) {
			button.setEnabled(false);
		}
		timer = new CountDownTimer();
	}
	
	public void leaveGame() {
		JButton jb = new JButton();
		jb.addActionListener(mTeamAction);
		jb.doClick();
		progressPanel.resetProgressBar();
	}
	
	public void freeze() {
		new FreezeTimer(2);
	}
	
	private class CountDownTimer extends Thread {
		private long startTime;
		private long currentTime;
		private int timePassed = -1;
		
		public CountDownTimer() {
			startTime = System.currentTimeMillis();
			currentTime = startTime;
			this.start();
		}
		
		public void run() {
			Font countDownFont = font.deriveFont(Font.BOLD, 150);
			instructionText.setFont(countDownFont);
			instructionText.setForeground(Color.BLUE);
			
			while(!timesUp()) {
				currentTime = System.currentTimeMillis();
				timePassed = (int) (currentTime-startTime)/1000;
				if(getTimeLeft()>0)
					setInstruction(""+getTimeLeft());
				else {
					countDownFont = countDownFont.deriveFont(Font.BOLD, 80);
					instructionText.setFont(countDownFont);
					instructionText.setForeground(Color.RED);
					setInstruction("START!");
				}
				instructionPanel.repaint();
				try {
					Thread.sleep(200);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			font = font.deriveFont(Font.PLAIN, 30);
			instructionText.setFont(font);
			instructionText.setForeground(Color.WHITE);
			for(JButton button : optionButtons) {
				button.setEnabled(true);
			}
			teamTimer.setTime(60);
		}
		
		public boolean timesUp() {
			return timePassed>=4;
		}
		
		public int getTimeLeft() {
			return 3-timePassed;
		}
	}
	
	private class FreezeTimer extends Thread {
		private long startTime;
		private long currentTime;
		private int timePassed = -1;
		private int freezeTime = -1;
		
		public FreezeTimer(int time) {
			startTime = System.currentTimeMillis();
			currentTime = startTime;
			freezeTime = time;
			this.start();
		}
		
		public void run() {
			for(JButton button : optionButtons) {
				button.setEnabled(false);
			}
			
			while(!timesUp()) {
				currentTime = System.currentTimeMillis();
				timePassed = (int) (currentTime-startTime)/1000;
				try {
					Thread.sleep(200);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			
			for(JButton button : optionButtons) {
				button.setEnabled(true);
			}
		}
		
		public boolean timesUp() {
			return timePassed>=freezeTime;
		}
		
		public int getTimeLeft() {
			return 3-timePassed;
		}
	}
	
	private class InstructionScreen extends JPanel{
		private static final long serialVersionUID = 1L;
		
		public InstructionScreen(){
			setPreferredSize(new Dimension(300, 300));
			setOpaque(false);
		}
/*		@Override
		public void paintComponent(Graphics g){
			super.paintComponent(g);
			
			g.drawImage(Constants.miniGameScreenImage, 0, 0, getWidth(), getHeight(), null);
		}*/
	}
	
	@Override
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		
		g.drawImage(Constants.miniGameBackground, 0, 0, getWidth(), getHeight(), null);
	}
	
	public void setInstruction(String instruction){
		instructionText.setText(instruction);
	}
}
