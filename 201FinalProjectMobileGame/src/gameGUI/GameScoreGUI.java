package gameGUI;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Image;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;

public class GameScoreGUI extends JPanel{

	private Border emptyBorder;
	private Font font;
	private JPanel patriotsPanel;
	private JPanel patriotsHolder;
	private JPanel traitorsHolder;
	private JPanel traitorsPanel;
	private JLabel patriotsLabel;
	private JLabel traitorsLabel;
	private CheckBoxPanel[] patriotsScorePanel;
	private CheckBoxPanel[] traitorsScorePanel;
	private int numGames = 5;
	private boolean patriotsWon = false;;
	private boolean traitorsWon = false;
	
	private int patriotsWins = -1;
	private int traitorsWins = -1;
	
	public GameScoreGUI(){
		this.setOpaque(false);
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		this.setPreferredSize(new Dimension(240, 100));
		emptyBorder = BorderFactory.createEmptyBorder();
		this.setBorder(emptyBorder);
		font = Constants.scoreFont;
		add(Box.createGlue());
		add(Box.createGlue());
		patriotsScorePanel = new CheckBoxPanel[5];
		traitorsScorePanel = new CheckBoxPanel[5];
		
		patriotsPanel = new JPanel();
		patriotsPanel.setPreferredSize(new Dimension(100,25));
		patriotsPanel.setOpaque(false);
		patriotsPanel.setBorder(emptyBorder);
		patriotsPanel.setLayout(new GridLayout(1,5));
		
		traitorsPanel = new JPanel();
		traitorsPanel.setPreferredSize(new Dimension(100,25));
		traitorsPanel.setBorder(emptyBorder);
		traitorsPanel.setOpaque(false);
		traitorsPanel.setLayout(new GridLayout(1,5));
		
		patriotsLabel = new JLabel(Constants.patriotsLabel);
		patriotsLabel.setFont(font);
		traitorsLabel = new JLabel(Constants.traitorsLabel);
		traitorsLabel.setFont(font);
		
		patriotsHolder = new JPanel();
		patriotsHolder.setBorder(emptyBorder);
		patriotsHolder.setOpaque(false);
		patriotsHolder.add(patriotsLabel);
		
		traitorsHolder = new JPanel();
		traitorsHolder.setBorder(emptyBorder);
		traitorsHolder.setOpaque(false);
		traitorsHolder.add(traitorsLabel);
		
		for(int i = 0; i<5; i++){
			patriotsScorePanel[i] = new CheckBoxPanel();
			patriotsScorePanel[i].setPatriot();
			patriotsPanel.add(patriotsScorePanel[i]);
			traitorsScorePanel[i] = new CheckBoxPanel();
			traitorsPanel.add(traitorsScorePanel[i]);
		}
		
		patriotsHolder.add(patriotsPanel);
		traitorsHolder.add(traitorsPanel);
		
		add(patriotsHolder);
		add(traitorsHolder);
		add(Box.createGlue());
		add(Box.createGlue());
	}
	
	class CheckBoxPanel extends JPanel{
		/**
		 * 
		 */
		private static final long serialVersionUID = 5833108884229512997L;
		private boolean checked = false;
		private boolean isPatriot = false;
		
		public CheckBoxPanel(){
			this.setOpaque(false);
		}
		@Override
		public void paintComponent(Graphics g){
			super.paintComponent(g);
			g.drawImage(Constants.scoreCheckBox, 0, 0, getWidth(), getHeight(), null);

			if(checked){
				if(isPatriot){
					g.drawImage(Constants.patriotsWinImage, 0, 0, getWidth(), getHeight(), null);
				}
				else {
					g.drawImage(Constants.traitorsWinImage, 0, 0, getWidth(), getHeight(), null);
				}
			}
		}
		
		public void setChecked(boolean b){
			checked = b;
		}
		
		public void setPatriot(){
			isPatriot = true;
		}
	}
	
	public void setPatriotsWin(){
		patriotsWins++;
		System.out.println("inPatriotrsWin");
		patriotsScorePanel[patriotsWins].setChecked(true);
		patriotsScorePanel[patriotsWins].repaint();
		//patriotsScorePanel[patriotsWins].setBackground(Color.black);
		//patriotsScorePanel[patriotsWins].setToDraw(Constants.patriotsWinImage);
	}
	
	public void setTraitorsWin(){
		traitorsWins++;
		System.out.println("inTraitorsWins");
		traitorsScorePanel[traitorsWins].setChecked(true);
		traitorsScorePanel[traitorsWins].repaint();
		//patriotsScorePanel[patriotsWins].setBackground(Color.black);
		//traitorsScorePanel[traitorsWins].setToDraw(Constants.traitorsWinImage);
	}
}
