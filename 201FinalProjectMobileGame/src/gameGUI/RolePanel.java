package gameGUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.util.Vector;

import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;

import customUI.ClearButton;
import customUI.PaintedPanel;
import gameGUI.GameWindow.SwitchPageAction;
import library.FontLibrary;

public class RolePanel extends PaintedPanel{
	private static final long serialVersionUID = 1L;
	
	private JPanel rolePanel;
	private JLabel roleLabel;
	private final String roleString = "TOP SECRET";
	private JLabel roleNameLabel;
	
	private JScrollPane nameSP;
	private static JTable nameTable;
	private static DefaultTableModel nameTableModel;
//	private String[] columns = {"Players"};
	
	private Vector<String> traitors;
	
	private JPanel buttonPanel;
	private ClearButton okButton;
	
	private SwitchPageAction mTeamAction;
	private JPanel mainPanel;
	private GameWindow gw;
	
	public RolePanel(SwitchPageAction teamAction, GameWindow gw) {
		this.gw = gw;
		mTeamAction = teamAction;
		traitors = new Vector<String>();
		initializeComponents();
		createGUI();
		addEvents();
	}

	private void initializeComponents() {
		mainPanel = new JPanel();
		rolePanel = new JPanel();
		roleLabel = new JLabel(roleString);
		roleNameLabel = new JLabel();
		
		nameTableModel = new DefaultTableModel();
		nameTable = new JTable(nameTableModel);
		nameSP = new JScrollPane(nameTable);
		
		buttonPanel = new JPanel();
		okButton = new ClearButton("Acknowledged",
				FontLibrary.getFont("resources/fonts/stencil.ttf", Font.BOLD, 24),
				Color.BLACK);
	}

	private void createGUI() {
		rolePanel.setLayout(new GridLayout(2,1));
		rolePanel.setOpaque(false);
		rolePanel.add(roleLabel);
		rolePanel.add(roleNameLabel);
		
		roleLabel.setFont(FontLibrary.getFont("resources/fonts/stencil.ttf", Font.BOLD, 50));
		roleLabel.setForeground(Color.BLACK);
		roleLabel.setHorizontalAlignment(JLabel.CENTER);
		roleNameLabel.setFont(FontLibrary.getFont("resources/fonts/stencil.ttf", Font.BOLD, 30));
		roleNameLabel.setForeground(Color.RED);
		roleNameLabel.setHorizontalAlignment(JLabel.CENTER);
		
		JTableHeader nameHeader = nameTable.getTableHeader();
		nameHeader.setPreferredSize(new Dimension(nameSP.getWidth(),40));
		nameHeader.setFont(FontLibrary.getFont("resources/fonts/typewriter.ttf", Font.PLAIN, 20));
		
		nameTable.setRowHeight(40);
		nameTable.setFont(FontLibrary.getFont("resources/fonts/typewriter.ttf", Font.PLAIN, 25));
		nameTable.setCellSelectionEnabled(false);
		nameTable.setGridColor(Color.BLACK);
		
		buttonPanel.setLayout(new GridBagLayout());
		buttonPanel.setOpaque(false);
		buttonPanel.setPreferredSize(new Dimension(540,50));
		buttonPanel.add(Box.createGlue());
		buttonPanel.add(okButton);
		
		mainPanel.setLayout(new BorderLayout());
		mainPanel.setOpaque(false);
		mainPanel.add(rolePanel,BorderLayout.NORTH);
		mainPanel.add(nameSP,BorderLayout.CENTER);
		mainPanel.add(buttonPanel,BorderLayout.SOUTH);
		
		add(mainPanel,BorderLayout.CENTER);
	}
	
	private void addEvents() {
		okButton.addActionListener(mTeamAction);
	}
	
	public void setRoleName(String roleName) {
		roleNameLabel.setText(roleName);
	}
	
	public void setTraitors(Vector<String> names) {
		traitors = names;
		displayTraitors();
	}
	
	private void displayTraitors(){
		if (gw.getIsTraitor()){
			nameTableModel.addColumn(new String("Traitor List"), traitors);
			setRoleName("Traitor");
		} else {
			setRoleName("Patriot");
			nameTable.setVisible(false);
			nameTable.setOpaque(false);
		}
	}
}

