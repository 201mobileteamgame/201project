package gameGUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import customUI.ClearButton;
import customUI.PaintedButton;
import customUI.PaintedTeamPanel;
import gameGUI.GameWindow.SwitchPageAction;
import library.FontLibrary;
import library.ImageLibrary;

public class TeamPanel extends PaintedTeamPanel {
private static final long serialVersionUID = 1L;
	
	private JPanel currLeaderPanel;
	private JLabel currLeaderLabel;
	private JPanel currLeaderHolder;
	private final String currLeaderString = "Leader:";
	private JLabel currLeaderNameLabel;
	private ClearButton leaderButton;
	
	private JPanel proposedTeamPanel;
	private JLabel proposedTeamLabel;
	private final String proposedTeamString = "Proposed Team:";
	private JPanel namePanel;
	private JLabel[] proposedNameLabels;
	private JLabel playerNameLabel;
	
	private JPanel buttonPanel;
	private ClearButton approveButton;
	private ClearButton rejectButton;
	
	private JPanel mainPanel;
	
	private SwitchPageAction mLeaderAction;
	private SwitchPageAction mMissionAction1;
	private SwitchPageAction mMissionAction2;
	private SwitchPageAction mMissionAction3;
	private SwitchPageAction mMissionAction4;
	private SwitchPageAction mSpectatorAction;
	private SwitchPageAction mRoleAction;
	private SwitchPageAction mStatsAction;
	private ActionEvent ae;
	
	private Vector<String> onMissionPlayers;
	
	private GameWindow gw;
	
	public TeamPanel(SwitchPageAction missionAction1, SwitchPageAction missionAction2,
			SwitchPageAction missionAction3, SwitchPageAction missionAction4,
			SwitchPageAction leaderAction, SwitchPageAction spectatorAction,
			SwitchPageAction roleAction, SwitchPageAction thankyouAction, GameWindow gw) {
		this.gw = gw;
		mMissionAction1 = missionAction1;
		mMissionAction2 = missionAction2;
		mMissionAction3 = missionAction3;
		mMissionAction4 = missionAction4;
		mLeaderAction = leaderAction;
		mSpectatorAction = spectatorAction;
		mRoleAction = roleAction;
		mStatsAction = thankyouAction;
		onMissionPlayers = new Vector<String>();
		initializeComponents();
		createGUI();
		addEvents();
	}

	private void initializeComponents() {
		mainPanel = new JPanel();
		currLeaderPanel = new JPanel();
		currLeaderLabel = new JLabel(currLeaderString);
		currLeaderNameLabel = new JLabel();
		currLeaderHolder = new JPanel();
		leaderButton = new ClearButton("Select Team",
				FontLibrary.getFont("resources/fonts/stencil.ttf", Font.BOLD, 35),
				Color.BLUE);
		
		proposedTeamPanel = new JPanel();
		proposedTeamLabel = new JLabel(proposedTeamString);
		proposedNameLabels = new JLabel[4];
		namePanel = new JPanel();
		for(int i=0;i<4;i++) {
			proposedNameLabels[i] = new JLabel();
		}
		playerNameLabel = new JLabel();
		
		buttonPanel = new JPanel();
		approveButton = new ClearButton("Approve",
				FontLibrary.getFont("resources/fonts/stencil.ttf", Font.BOLD, 30),
				Color.GREEN);
		rejectButton = new ClearButton("Reject",
				FontLibrary.getFont("resources/fonts/stencil.ttf", Font.BOLD, 30),
				Color.RED);

	}
	
	public TimerLogsPanel getTimerPanel() {return (TimerLogsPanel)topPanel;}

	private void createGUI() {
		this.setOpaque(false);
		
		topPanel = new TimerLogsPanel(gw);
		topPanel.setOpaque(false);
		add(topPanel, BorderLayout.NORTH);
		
		bottomPanel = new TopSecretPanel(mRoleAction);
		bottomPanel.setOpaque(false);
		add(bottomPanel,BorderLayout.SOUTH);
		
		currLeaderPanel.setLayout(new GridLayout(3,1));
		currLeaderPanel.setOpaque(false);
		currLeaderPanel.setPreferredSize(new Dimension(540,150));
		currLeaderHolder.add(currLeaderLabel);
		currLeaderHolder.add(currLeaderNameLabel);
		currLeaderHolder.setOpaque(false);
		currLeaderPanel.add(currLeaderHolder);
		currLeaderNameLabel.setText("TestKeader");
		currLeaderPanel.add(leaderButton);
		
		currLeaderLabel.setFont(FontLibrary.getFont("resources/fonts/stencil.ttf", Font.BOLD, 30));
		currLeaderLabel.setForeground(Color.BLACK);
		currLeaderLabel.setHorizontalAlignment(JLabel.CENTER);
		currLeaderNameLabel.setFont(FontLibrary.getFont("resources/fonts/stencil.ttf", Font.BOLD, 30));
		currLeaderNameLabel.setForeground(Color.RED);
		currLeaderNameLabel.setHorizontalAlignment(JLabel.CENTER);
		
		proposedTeamPanel.setLayout(new BorderLayout());
		proposedTeamPanel.setOpaque(false);
		proposedTeamLabel.setFont(FontLibrary.getFont("resources/fonts/stencil.ttf", Font.BOLD, 30));
		proposedTeamPanel.add(proposedTeamLabel, BorderLayout.NORTH);
		
		namePanel.setLayout(new GridLayout(2,2));
		namePanel.setOpaque(false);
		for(JLabel nameLabel : proposedNameLabels) {
			nameLabel.setFont(FontLibrary.getFont("resources/fonts/stencil.ttf", Font.BOLD, 25));
			nameLabel.setForeground(Color.GRAY);
			namePanel.add(nameLabel);
		}
		proposedTeamPanel.add(namePanel, BorderLayout.CENTER);
		
		playerNameLabel.setFont(FontLibrary.getFont("resources/fonts/stencil.ttf", Font.BOLD, 20));
		playerNameLabel.setForeground(Color.BLACK);
		proposedTeamPanel.add(playerNameLabel, BorderLayout.SOUTH);
		
		approveButton.setForeground(Color.GREEN);
		rejectButton.setForeground(Color.RED);
		enableAllButtons(false);
		
		buttonPanel.setLayout(new GridLayout(1,2));
		buttonPanel.setOpaque(false);
		buttonPanel.setPreferredSize(new Dimension(140,150));
		buttonPanel.add(approveButton);
		buttonPanel.add(rejectButton);
		
		mainPanel.setLayout(new BorderLayout());
		mainPanel.setOpaque(false);
		mainPanel.add(currLeaderPanel,BorderLayout.NORTH);
		mainPanel.add(proposedTeamPanel, BorderLayout.CENTER);
		mainPanel.add(buttonPanel,BorderLayout.SOUTH);
		
		add(mainPanel, BorderLayout.CENTER);
	}
	
	private void addEvents() {
		approveButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ae = e;
				approveButton.setEnabled(false);
				rejectButton.setEnabled(false);
				gw.getClient().sendMessage("Vote: Approve");
	//			gw.startAllMission();
			}
		});
		
		
		
		rejectButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ae = e;
				approveButton.setEnabled(false);
				rejectButton.setEnabled(false);
				gw.getClient().sendMessage("Vote: Reject");
			}
		});
		
//		rejectButton.addActionListener(mLeaderAction);
		
		leaderButton.addActionListener(mLeaderAction);
	}
	
	public void setPlayerIdentity(String name) {
		playerNameLabel.setText("You are: "+name);
	}
	
/*	public void enterMissionPanel() {
		JButton jb = new JButton();
		jb.addActionListener(mMissionAction1);
		jb.doClick();
		jb.removeActionListener(mMissionAction1);
		gw.startAllMission();
	}*/
	
	public void enableAllButtons(boolean enable) {
		approveButton.setEnabled(enable);
		rejectButton.setEnabled(enable);
	}
	
	public void setLeaderName(String currLeaderName) {
		System.out.println("setting leader name");
		leaderButton.setEnabled(false);
		currLeaderNameLabel.setText(currLeaderName);
		System.out.println("I am player: "+gw.getClient().getPlayer().getPlayerName());
		if(gw.getClient().getPlayer().getPlayerName().equals(currLeaderName)) {
			System.out.println("reach set leader true");
			leaderButton.setEnabled(true);
		}
		
	}
	
	public void clearLeader() {
		currLeaderNameLabel.setText("");
	}
	
	public void clearTeam() {
		for(JLabel label : proposedNameLabels) {
			label.setText("");
		}
		onMissionPlayers.removeAllElements();
	}
	
	public void setProposedTeam(String name) {
		onMissionPlayers.add(name);
	}
	
	public void startMission() {
		boolean enteredMission = false;
		int order = 0;
		for(String name : onMissionPlayers) {
			if((gw.getClient().getPlayer().getPlayerName()).equals(name)) {
				gw.startAllMission();
				if(order==0) {
					mMissionAction1.actionPerformed(ae);
				} else if(order==1) {
					mMissionAction2.actionPerformed(ae);
				} else if(order==2) {
					mMissionAction3.actionPerformed(ae);
				} else if(order==3) {
					mMissionAction4.actionPerformed(ae);
				}
				enteredMission = true;
			}
			order+=1;
		}
		if(!enteredMission) {
			mSpectatorAction.actionPerformed(ae);
		}
	}
	
	public void displayProposedTeam() {
		int count = 0;
		for(String name : onMissionPlayers) {
			proposedNameLabels[count].setText(name);
			count++;
		}
		leaderButton.setEnabled(false);
	}
	
	public void enterFinalPanel() {
		//TODO
		JButton jb = new JButton();
		jb.addActionListener(mStatsAction);
		jb.doClick();
	}
	
}


