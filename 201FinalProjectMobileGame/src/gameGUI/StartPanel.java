package gameGUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.JPanel;

import customUI.PaintedButton;
import customUI.PaintedPanel;
import gameGUI.GameWindow.SwitchPageAction;
import library.FontLibrary;
import library.ImageLibrary;

public class StartPanel extends PaintedPanel {

	private static final long serialVersionUID = 1L;
	private JLabel titleLabel1;
	private JLabel titleLabel2;
	private PaintedButton hostButton;
	private PaintedButton joinButton;
	
	private SwitchPageAction mHostAction;
	private SwitchPageAction mJoinAction;
	
	private SwitchPageAction mRecordsAction, mLoginAction;
	
	private JPanel mainPanel;
	
	private GameWindow gw;
	
	public StartPanel(SwitchPageAction hostAction, SwitchPageAction joinAction, SwitchPageAction inRecordsAction, SwitchPageAction inLoginAction, GameWindow inWindow) {
		mHostAction = hostAction;
		mJoinAction = joinAction;
		mRecordsAction = inRecordsAction;
		mLoginAction = inLoginAction;
		gw = inWindow;
		initializeComponents();
		createGUI();
		addEvents();
	}
	
	private void initializeComponents() {
		mainPanel = new JPanel();
		titleLabel1 = new JLabel(Constants.titleString1);
		titleLabel2 = new JLabel(Constants.titleString2);
		
		hostButton = new PaintedButton(Constants.hostString,
				FontLibrary.getFont("resources/fonts/typewriter.ttf", Font.BOLD, 20),
				ImageLibrary.getImage("resources/images/buttons/redButton.png")
				);
		joinButton = new PaintedButton(Constants.joinString,
				FontLibrary.getFont("resources/fonts/typewriter.ttf", Font.BOLD, 20),
				ImageLibrary.getImage("resources/images/buttons/redButton.png")
				);
	}
	
	private void createGUI() {
		topPanel = new PlayerInfoOptionsPanel(mRecordsAction, mLoginAction);
		topPanel.setOpaque(false);
		add(topPanel, BorderLayout.NORTH);
		
		mainPanel.setLayout(new GridBagLayout());
		mainPanel.setOpaque(false);

		titleLabel1.setFont(FontLibrary.getFont("resources/fonts/stencil.ttf", Font.PLAIN, 64));
		titleLabel1.setForeground(Color.RED);
		titleLabel1.setHorizontalAlignment(JLabel.CENTER);
		titleLabel1.setAlignmentX(CENTER_ALIGNMENT);
		
		titleLabel2.setFont(FontLibrary.getFont("resources/fonts/stencil.ttf", Font.PLAIN, 64));
		titleLabel2.setForeground(Color.RED);
		titleLabel2.setHorizontalAlignment(JLabel.CENTER);
		titleLabel2.setAlignmentX(CENTER_ALIGNMENT);
		
		JPanel spacingPanel = new JPanel();
		spacingPanel.setOpaque(false);

		GridBagConstraints gbc = new GridBagConstraints();
		gbc.ipady = 125;
		mainPanel.add(spacingPanel, gbc);
		gbc.ipady = 0;
		gbc.gridy = 1;
		gbc.gridwidth = 2;
		gbc.weightx = 1;
		mainPanel.add(titleLabel1, gbc);
		gbc.gridy = 2;
		mainPanel.add(titleLabel2, gbc);
		gbc.weighty = .5;
		gbc.gridwidth = 1;
		gbc.gridy = 3;
		gbc.ipadx = 60;
		gbc.ipady = 20;
		mainPanel.add(hostButton, gbc);
		gbc.gridx = 1;
		mainPanel.add(joinButton, gbc);
		
		add(mainPanel,BorderLayout.CENTER);
	}
	
	private void addEvents() {
		hostButton.addActionListener(mHostAction);
		hostButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				gw.getLoginPanel().setFromPanel(1);
				gw.getInstructionsPanel().setFromPanel(1);
				
			}
			
		});
		joinButton.addActionListener(mJoinAction);
		joinButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				gw.getLoginPanel().setFromPanel(2);
				gw.getInstructionsPanel().setFromPanel(2);
			}
			
		});
	}
	
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		g.drawImage(Constants.spyImage, getWidth()/3, getHeight()/4, getWidth()/3, getHeight()/3, null);
	}
	
}
