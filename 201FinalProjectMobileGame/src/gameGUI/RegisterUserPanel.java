package gameGUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import customUI.PaintedButton;
import customUI.PaintedPanel;
import database.DatabaseClient;
import gameGUI.GameWindow.SwitchPageAction;
import library.FontLibrary;
import library.ImageLibrary;

//TODO: change passwords to JPasswordField
//TODO: change to case-insensitivity
//TODO: link registration to database

public class RegisterUserPanel extends PaintedPanel {
	
private static final long serialVersionUID = 1L;
	
	private JLabel usernameLabel, passwordLabel, confirmationLabel, hintLabel, errorLabel;
	private JTextField usernameTF, passwordTF, confirmationTF, hintTF;
	private PaintedButton registerButton, cancelButton;
	
	private SwitchPageAction registerAction;
	
	private GameWindow gw;
	private JPanel mainPanel;
	DatabaseClient client;
	
	public RegisterUserPanel(SwitchPageAction inRegisterAction, DatabaseClient dbClient, GameWindow inWindow) {
		registerAction = inRegisterAction;
		client = dbClient;
		gw = inWindow;
		initializeComponents();
		createGUI();
		addEvents();
	}
	
	private void initializeComponents() {
		mainPanel = new JPanel();
		
		usernameLabel = new JLabel(Constants.usernameString);
		usernameLabel.setFont(FontLibrary.getFont("resources/fonts/typewriter.ttf", Font.BOLD, 20));
		
		passwordLabel = new JLabel(Constants.passwordString);
		passwordLabel.setFont(FontLibrary.getFont("resources/fonts/typewriter.ttf", Font.BOLD, 20));
		
		confirmationLabel = new JLabel(Constants.confirmString);
		confirmationLabel.setFont(FontLibrary.getFont("resources/fonts/typewriter.ttf", Font.BOLD, 20));
		
		hintLabel = new JLabel(Constants.hintString);
		hintLabel.setFont(FontLibrary.getFont("resources/fonts/typewriter.ttf", Font.BOLD, 20));
		
		usernameTF = new JTextField(5);
		usernameTF.setFont(FontLibrary.getFont("resources/fonts/typewriter.ttf", Font.PLAIN, 20));
		usernameTF.setOpaque(false);
		passwordTF = new JPasswordField(5);
		passwordTF.setFont(FontLibrary.getFont("resources/fonts/typewriter.ttf", Font.PLAIN, 20));
		passwordTF.setOpaque(false);
		confirmationTF = new JPasswordField(5);
		confirmationTF.setFont(FontLibrary.getFont("resources/fonts/typewriter.ttf", Font.PLAIN, 20));
		confirmationTF.setOpaque(false);
		hintTF = new JTextField(5);
		hintTF.setFont(FontLibrary.getFont("resources/fonts/typewriter.ttf", Font.PLAIN, 20));
		hintTF.setOpaque(false);
		
		registerButton = new PaintedButton(Constants.registerString,
				FontLibrary.getFont("resources/fonts/typewriter.ttf", Font.BOLD, 14),
				ImageLibrary.getImage("resources/images/buttons/redButton.png")
				);
		cancelButton = new PaintedButton(Constants.cancelString,
				FontLibrary.getFont("resources/fonts/typewriter.ttf", Font.BOLD, 14),
				ImageLibrary.getImage("resources/images/buttons/redButton.png")
				);
		
		registerButton.setEnabled(false);
		
		errorLabel = new JLabel("");
		errorLabel.setFont(FontLibrary.getFont("resources/fonts/typewriter.ttf", Font.BOLD, 20));
		errorLabel.setForeground(Color.RED);
	}
	
	private void createGUI() {
		mainPanel.setLayout(new GridBagLayout());
		
		GridBagConstraints gbc = new GridBagConstraints();
		
		mainPanel.add(usernameLabel, gbc);
		gbc.gridx = 1;
		gbc.ipadx = 150;
		mainPanel.add(usernameTF, gbc);
		gbc.ipadx = 0;
		gbc.gridy = 1;
		gbc.gridx = 0;
		
		mainPanel.add(passwordLabel, gbc);
		gbc.gridx = 1;
		gbc.ipadx = 150;
		mainPanel.add(passwordTF, gbc);
		gbc.ipadx = 0;
		gbc.gridy = 2;
		gbc.gridx = 0;
		
		mainPanel.add(confirmationLabel, gbc);
		gbc.gridx = 1;
		gbc.ipadx = 150;
		mainPanel.add(confirmationTF, gbc);
		gbc.ipadx = 0;
		gbc.gridy = 3;
		gbc.gridx = 0;
		
		mainPanel.add(hintLabel, gbc);
		gbc.gridx = 1;
		gbc.ipadx = 150;
		mainPanel.add(hintTF, gbc);
		gbc.ipadx = 0;
		gbc.gridy = 4;
		gbc.gridx = 0;
		
		gbc.gridwidth = 2;
		mainPanel.add(registerButton, gbc);
		gbc.gridy = 5;
		
		mainPanel.add(cancelButton, gbc);
		
		mainPanel.setOpaque(false);
		
		add(mainPanel,BorderLayout.CENTER);
	}
	
	public void registerUser(String username, String password, String passHint)
	{
		gw.getDatabaseClient().sendObject("REGISTER:"+username+"/"+password+"/"+passHint);
		System.out.println("Registering use from panel");
	}
	
	private void addEvents() {
		registerButton.addActionListener(registerAction);
		registerButton.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println("Registration Attempt with USERNAME: " + usernameTF.getText() + " PASSWORD: " + confirmationTF.getText() + " HINT: " + hintTF.getText());
				registerUser(""+usernameTF.getText(),""+confirmationTF.getText(),""+hintTF.getText());
				System.out.println("Registering user from panel action listner");
			}
		});
		
		cancelButton.addActionListener(registerAction);
		
		usernameTF.getDocument().addDocumentListener(new DocumentListener() {
			public void changedUpdate(DocumentEvent e) {
				changed();
			}
			public void removeUpdate(DocumentEvent e) {
				changed();
			}
			public void insertUpdate(DocumentEvent e) {
				changed();
			}

			public void changed() {
				if (usernameTF.getText().equals("") || passwordTF.getText().equals("") || confirmationTF.getText().equals("") || hintTF.getText().equals("") || !passwordTF.getText().equals(confirmationTF.getText())){
					registerButton.setEnabled(false);
				}
				else {
					registerButton.setEnabled(true);
				}
			}
		});
		
		passwordTF.getDocument().addDocumentListener(new DocumentListener() {
			public void changedUpdate(DocumentEvent e) {
				changed();
			}
			public void removeUpdate(DocumentEvent e) {
				changed();
			}
			public void insertUpdate(DocumentEvent e) {
				changed();
			}

			public void changed() {
				if (usernameTF.getText().equals("") || passwordTF.getText().equals("") || confirmationTF.getText().equals("") || hintTF.getText().equals("") || !passwordTF.getText().equals(confirmationTF.getText())){
					registerButton.setEnabled(false);
				}
				else {
					registerButton.setEnabled(true);
				}
			}
		});
		
		confirmationTF.getDocument().addDocumentListener(new DocumentListener() {
			public void changedUpdate(DocumentEvent e) {
				changed();
			}
			public void removeUpdate(DocumentEvent e) {
				changed();
			}
			public void insertUpdate(DocumentEvent e) {
				changed();
			}

			public void changed() {
				if (usernameTF.getText().equals("") || passwordTF.getText().equals("") || confirmationTF.getText().equals("") || hintTF.getText().equals("") || !passwordTF.getText().equals(confirmationTF.getText())){
					registerButton.setEnabled(false);
				}
				else {
					registerButton.setEnabled(true);
				}
			}
		});
		
		hintTF.getDocument().addDocumentListener(new DocumentListener() {
			public void changedUpdate(DocumentEvent e) {
				changed();
			}
			public void removeUpdate(DocumentEvent e) {
				changed();
			}
			public void insertUpdate(DocumentEvent e) {
				changed();
			}

			public void changed() {
				if (usernameTF.getText().equals("") || passwordTF.getText().equals("") || confirmationTF.getText().equals("") || hintTF.getText().equals("") || !passwordTF.getText().equals(confirmationTF.getText())){
					registerButton.setEnabled(false);
				}
				else {
					registerButton.setEnabled(true);
				}
			}
		});
	}
	
	public void clearTF(){
		usernameTF.setText("");
		passwordTF.setText("");
		confirmationTF.setText("");
		hintTF.setText("");
	}
}
