package gameGUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;

import customUI.PaintedPanel;
import gameGUI.GameWindow.SwitchPageAction;

public class SpectatorPanel extends PaintedPanel {
	private static final long serialVersionUID = 1L;
	
	Font font;
	private JButton[] optionButtons;
	private final int numOptions = 4;
	Border emptyBorder;
	
	JLabel instructionText;
	JPanel mainPanel;
	InstructionScreen instructionPanel;
	CountDownTimer timer = null;
	TimerGUI teamTimer = null;
	
	private JPanel topEmptyPanel;
	private JPanel bottomEmptyPanel;
	private JPanel westEmptyPanel;
	private JPanel eastEmptyPanel;
	
SwitchPageAction mTeamAction;
	
	private GameWindow gw;
	
	public SpectatorPanel(SwitchPageAction spa, GameWindow gw) {
		this.gw = gw;
		mTeamAction = spa;
		emptyBorder = BorderFactory.createEmptyBorder();
		font = Constants.miniGameScreenFont;
		
		teamTimer = new TimerGUI();
		
		topEmptyPanel = new JPanel();
		topEmptyPanel.setPreferredSize(Constants.topEmptyPanelDimension);
		topEmptyPanel.setLayout(new GridLayout(1,3));
		topEmptyPanel.add(teamTimer);
		topEmptyPanel.setBorder(new EmptyBorder(10,15,10,15));
		topEmptyPanel.add(new JLabel());
		topEmptyPanel.add(new JLabel());
		topPanel.setOpaque(false);
		topEmptyPanel.setOpaque(false);
		bottomEmptyPanel = new JPanel();
		bottomEmptyPanel.setPreferredSize(Constants.bottomEmptyPanelDimension);
		bottomEmptyPanel.setOpaque(false);
		westEmptyPanel = new JPanel();
		westEmptyPanel.setPreferredSize(Constants.westEmptyPanelDimension);
		westEmptyPanel.setOpaque(false);
		eastEmptyPanel = new JPanel();
		eastEmptyPanel.setPreferredSize(Constants.eastEmptyPanelDimension);
		eastEmptyPanel.setOpaque(false);
		
		mainPanel = new JPanel();
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.PAGE_AXIS));
		mainPanel.setBorder(emptyBorder);
		mainPanel.setBackground(new Color(0,0,0,0));
		
		instructionPanel = new InstructionScreen();
		instructionPanel.setBackground(new Color(0,0,0,0));
		instructionPanel.setLayout(new BorderLayout());
		
		instructionText = new JLabel();
		instructionText.setFont(font);
		instructionText.setHorizontalAlignment(SwingConstants.CENTER);
		instructionText.setForeground(Color.WHITE);
		
		instructionPanel.add(instructionText);
		mainPanel.add(instructionPanel);
		
		//The middle of the panel, the answers buttons
		JPanel centerPanel = new JPanel(new GridLayout(2,2,5,5));
		centerPanel.setBackground(new Color(0,0,0,0));
		centerPanel.setBorder(new EmptyBorder(50,0,0,0));
		optionButtons = new JButton[numOptions];
		font = Constants.miniGameButtonsFont;
/*			for(int i = 0; i < numOptions; ++i) {
				optionButtons[i] = new JButton();
				optionButtons[i].setIcon(Constants.redButtonIcon);
				optionButtons[i].setPressedIcon(Constants.redButtonPressedIcon);
				optionButtons[i].setHorizontalTextPosition(JButton.CENTER);
				optionButtons[i].setVerticalTextPosition(JButton.CENTER);

				optionButtons[i].setBorder(emptyBorder);
				optionButtons[i].setEnabled(false);
				centerPanel.add(optionButtons[i]);
			}*/
		for(int i=0;i<4;i++) {
			centerPanel.add(new JLabel());
		}
		mainPanel.add(centerPanel);
		mainPanel.setOpaque(false);
		add(mainPanel, BorderLayout.CENTER);
		add(topEmptyPanel,BorderLayout.NORTH);
		add(bottomEmptyPanel,BorderLayout.SOUTH);
		add(westEmptyPanel,BorderLayout.WEST);
		add(eastEmptyPanel,BorderLayout.EAST);
	}
	
	public void startGame() {
		setInstruction(" ");
		timer = new CountDownTimer();
	}
	
	public void leaveGame() {
		JButton jb = new JButton();
		jb.addActionListener(mTeamAction);
		jb.doClick();
		this.setInstruction("");
	}
	
	public void freeze() {
		new FreezeTimer(2);
	}
	
	public void showMisclick(String name) {
		this.setInstruction(name+" misclicked");
	}
	
	private class CountDownTimer extends Thread {
		private long startTime;
		private long currentTime;
		private int timePassed = -1;
		
		public CountDownTimer() {
			startTime = System.currentTimeMillis();
			currentTime = startTime;
			this.start();
		}
		
		public void run() {
			Font countDownFont = font.deriveFont(Font.BOLD, 150);
			instructionText.setFont(countDownFont);
			instructionText.setForeground(Color.BLUE);
			
			while(!timesUp()) {
				currentTime = System.currentTimeMillis();
				timePassed = (int) (currentTime-startTime)/1000;
				if(getTimeLeft()>0)
					setInstruction(""+getTimeLeft());
				else {
					countDownFont = countDownFont.deriveFont(Font.BOLD, 80);
					instructionText.setFont(countDownFont);
					instructionText.setForeground(Color.RED);
					setInstruction("START1!");
				}
				instructionPanel.repaint();
				try {
					Thread.sleep(200);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			instructionText.setFont(font);
			instructionText.setForeground(Color.WHITE);
			teamTimer.setTime(60);
		}
		
		public boolean timesUp() {
			return timePassed>=4;
		}
		
		public int getTimeLeft() {
			return 3-timePassed;
		}
	}
	
	private class FreezeTimer extends Thread {
		private long startTime;
		private long currentTime;
		private int timePassed = -1;
		private int freezeTime = -1;
		
		public FreezeTimer(int time) {
			startTime = System.currentTimeMillis();
			currentTime = startTime;
			freezeTime = time;
			this.start();
		}
		
		public void run() {
			while(!timesUp()) {
				currentTime = System.currentTimeMillis();
				timePassed = (int) (currentTime-startTime)/1000;
				try {
					Thread.sleep(200);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		
		public boolean timesUp() {
			return timePassed>=freezeTime;
		}
	}
	
	private class InstructionScreen extends JPanel{
		private static final long serialVersionUID = 1L;
		
		public InstructionScreen(){
			setPreferredSize(new Dimension(300, 300));
			setOpaque(false);
		}
/*		@Override
		public void paintComponent(Graphics g){
			super.paintComponent(g);
			
			g.drawImage(Constants.miniGameScreenImage, 0, 0, getWidth(), getHeight(), null);
		}*/
	}
	
	@Override
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		
		g.drawImage(Constants.miniGameBackground, 0, 0, getWidth(), getHeight(), null);
	}
	
	public void setInstruction(String instruction){
		instructionText.setText(instruction);
	}
}
