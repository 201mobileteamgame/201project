package gameGUI;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;

import customUI.PaintedPanel;
import library.FontLibrary;
import library.ImageLibrary;

public class OptionsWindow extends JFrame {

	JFrame parent;
	
	{
		setUndecorated(true);
	}
	
	OptionsWindow(JFrame p) {
		parent = p;
		setSize(new Dimension(parent.getWidth()-100, parent.getHeight()-400));
		
		PaintedPanel jp = new PaintedPanel();
		jp.setBG(ImageLibrary.getImage("resources/images/tempOptionsBG.jpg"));
		jp.setLayout(new BoxLayout(jp, BoxLayout.Y_AXIS));
		
		//Labels
		JLabel volumeLabel = new JLabel("Volume");
		volumeLabel.setFont(FontLibrary.getFont("resources/fonts/typewriter.ttf", Font.PLAIN, 24));
		volumeLabel.setForeground(Color.WHITE);
		
		JLabel musicLabel = new JLabel("Music");
		musicLabel.setFont(FontLibrary.getFont("resources/fonts/typewriter.ttf", Font.PLAIN, 18));
		musicLabel.setForeground(Color.WHITE);
		
		JLabel sfxLabel = new JLabel("SFX");
		sfxLabel.setFont(FontLibrary.getFont("resources/fonts/typewriter.ttf", Font.PLAIN, 18));
		sfxLabel.setForeground(Color.WHITE);
		
		//Sliders
		JSlider musicSlider = new JSlider(JSlider.HORIZONTAL);
		musicSlider.setOpaque(false);
		musicSlider.setForeground(Color.RED);
		JSlider sfxSlider = new JSlider(JSlider.HORIZONTAL);
		sfxSlider.setOpaque(false);
		sfxSlider.setForeground(Color.RED);
		
		jp.add(Box.createGlue());
		jp.add(volumeLabel);
		jp.add(musicLabel);
		jp.add(musicSlider);
		jp.add(sfxLabel);
		jp.add(sfxSlider);
		jp.add(Box.createGlue());
		add(jp);
		
	}
	
	public void appear(){
		setLocation(parent.getX()+50, parent.getY()+200);
		setVisible(true);
	}
}
