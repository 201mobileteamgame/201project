package gameGUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.Box;
import javax.swing.DefaultListSelectionModel;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;

import customUI.ClearButton;
import customUI.PaintedButton;
import customUI.PaintedPanel;
import gameGUI.GameWindow.SwitchPageAction;
import library.FontLibrary;
import library.ImageLibrary;

public class LeaderSelectPanel extends PaintedPanel{
	private static final long serialVersionUID = 1L;
	
	private JPanel leaderPanel;
	private JLabel leaderLabel;
	private final String leaderString = "Leader";
//	private JLabel leaderNameLabel;
	
	private JScrollPane nameSP;
	private static JTable nameTable;
	private static DefaultTableModel nameTableModel;
	private DefaultListSelectionModel selectionModel;
	private String[] columns = {"Players"};
	
	private JPanel buttonPanel;
	private ClearButton selectButton;
	private JPanel mainPanel;
	
	private Vector<String> teamVector;
	
	private JPanel topEmptyPanel;
	private JPanel bottomEmptyPanel;
	private JPanel westEmptyPanel;
	private JPanel eastEmptyPanel;
	
	private Vector<String> players;
	
	private SwitchPageAction mTeamAction;
	
	private GameWindow gw;
	
	public LeaderSelectPanel(SwitchPageAction spa, GameWindow gw) {
		this.gw = gw;
		mTeamAction = spa;
		players = new Vector<String>();
		teamVector = new Vector<String>();
		initializeComponents();
		createGUI();
		addEvents();
	}

	private void initializeComponents() {
		mainPanel = new JPanel();
		leaderPanel = new JPanel();
		leaderLabel = new JLabel(leaderString);
//		leaderNameLabel = new JLabel();
		
		nameTableModel = new DefaultTableModel(columns,0){
			private static final long serialVersionUID = 1L;
			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		nameTable = new JTable(nameTableModel);
		selectionModel = (DefaultListSelectionModel) nameTable.getSelectionModel();
		nameSP = new JScrollPane(nameTable);
		
		buttonPanel = new JPanel();
		selectButton = new ClearButton("Select",
				FontLibrary.getFont("resources/fonts/stencil.ttf", Font.BOLD, 24),
				Color.BLACK);
		
		topEmptyPanel = new JPanel();
		topEmptyPanel.setPreferredSize(Constants.topEmptyPanelDimension);
		topEmptyPanel.setOpaque(false);
		bottomEmptyPanel = new JPanel();
		bottomEmptyPanel.setPreferredSize(Constants.bottomEmptyPanelDimension);
		bottomEmptyPanel.setOpaque(false);
		westEmptyPanel = new JPanel();
		westEmptyPanel.setPreferredSize(Constants.westEmptyPanelDimension);
		westEmptyPanel.setOpaque(false);
		eastEmptyPanel = new JPanel();
		eastEmptyPanel.setPreferredSize(Constants.eastEmptyPanelDimension);
		eastEmptyPanel.setOpaque(false);	
	}

	private void createGUI() {
		this.setOpaque(false);
		
		mainPanel.setLayout(new BorderLayout());
		mainPanel.setOpaque(false);

		leaderPanel.setLayout(new GridLayout(1,1));
		leaderPanel.setOpaque(false);
		leaderPanel.add(leaderLabel);
//		leaderPanel.add(leaderNameLabel);
		
		leaderLabel.setFont(FontLibrary.getFont("resources/fonts/stencil.ttf", Font.BOLD, 50));
		leaderLabel.setForeground(Color.BLACK);
		leaderLabel.setHorizontalAlignment(JLabel.CENTER);
//		leaderNameLabel.setFont(FontLibrary.getFont("resources/fonts/stencil.ttf", Font.BOLD, 30));
//		leaderNameLabel.setForeground(Color.RED);
//		leaderNameLabel.setHorizontalAlignment(JLabel.CENTER);
		
		JTableHeader nameHeader = nameTable.getTableHeader();
		nameHeader.setPreferredSize(new Dimension(nameSP.getWidth(),40));
		nameHeader.setFont(FontLibrary.getFont("resources/fonts/typewriter.ttf", Font.PLAIN, 20));
		
		nameTable.setRowHeight(40);
		nameTable.setFont(FontLibrary.getFont("resources/fonts/typewriter.ttf", Font.PLAIN, 25));
		nameTable.setCellSelectionEnabled(false);
		nameTable.setGridColor(Color.BLACK);
		
		nameTable.setRowSelectionAllowed(true);
		nameTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		
		buttonPanel.setLayout(new GridLayout(1,3));
		buttonPanel.setOpaque(false);
		buttonPanel.setPreferredSize(new Dimension(540,30));
		buttonPanel.add(Box.createGlue());
		buttonPanel.add(Box.createGlue());
		buttonPanel.add(selectButton);
		
		mainPanel.add(leaderPanel,BorderLayout.NORTH);
		mainPanel.add(nameSP,BorderLayout.CENTER);
		mainPanel.add(buttonPanel,BorderLayout.SOUTH);
		
		add(topEmptyPanel,BorderLayout.NORTH);
		add(bottomEmptyPanel,BorderLayout.SOUTH);
		add(westEmptyPanel,BorderLayout.WEST);
		add(eastEmptyPanel,BorderLayout.EAST);
		add(mainPanel, BorderLayout.CENTER);
	}
	
	private void addEvents() {
	//	selectButton.addActionListener(mTeamAction);
		selectButton.addActionListener(new ActionListener() {
			@Override
	        public void actionPerformed(ActionEvent e) {
				String leaderTeam = "LeaderTeam:";
				int numSelected = 0;
	            for (int i = 0; i < nameTableModel.getRowCount(); i++) {
	                if (selectionModel.isSelectedIndex(i)) {
	                    String selectedPlayer = (String) nameTableModel.getValueAt(i, 0);
	                    leaderTeam = leaderTeam+" "+selectedPlayer;
	                    numSelected++;
	                }
	            }
	            if(numSelected==4) {
	            	gw.getClient().sendMessage(leaderTeam);
	            	//TODO Show error label
	            	mTeamAction.actionPerformed(e);
	            }
	        }
		});
	}
	
//	public void setLeaderName(String leaderName) {
//		leaderNameLabel.setText(leaderName);
//	}
	
	public void setPlayerList(Vector<String> playerList) {
		nameTableModel.setRowCount(0);
		players = playerList;
		int count = 0;
		for(String playerName : playerList) {
			Object[] row = {playerName};
			nameTableModel.insertRow(count, row);
			count++;
		}
	}
	
	
}
