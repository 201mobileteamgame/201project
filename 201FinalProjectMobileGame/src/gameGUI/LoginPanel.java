package gameGUI;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import customUI.PaintedButton;
import customUI.PaintedPanel;
import database.DatabaseClient;
import game.Player;
import gameGUI.GameWindow.SwitchPageAction;
import library.FontLibrary;
import library.ImageLibrary;

//TODO: link checking to database
//TODO: add hint text after failing password check

public class LoginPanel extends PaintedPanel{

	private static final long serialVersionUID = 1L;
	
	private JLabel usernameLabel, passwordLabel, registerPromptLabel, errorLabel;
	private JTextField usernameTF, passwordTF;
	
	private PaintedButton loginButton, registerButton, cancelButton;
	DatabaseClient client;
	
	private ActionListener mLoginAction;
	private SwitchPageAction mRegisterAction;
	
	private GameWindow gw;
	
	private JPanel mainPanel;
	
	private int fromPanel = 0;
	
	public void setFromPanel(int panel){
		fromPanel = panel;
	}
	
	public LoginPanel(ActionListener inLoginAction, SwitchPageAction inRegisterAction, DatabaseClient dbClient, GameWindow inWindow) {
		mLoginAction = inLoginAction;
		mRegisterAction = inRegisterAction;
		initializeComponents();
		createGUI();
		addEvents();
		gw = inWindow;
		String input = JOptionPane.showInputDialog(this  ,"Type in the database IP:");
		client = dbClient;
		client = new DatabaseClient(input,gw);
		gw.setDatabaseClient(client);
	}
	
	private void initializeComponents() {
		mainPanel = new JPanel();
		
		usernameLabel = new JLabel(Constants.usernameString);
		usernameLabel.setFont(FontLibrary.getFont("resources/fonts/typewriter.ttf", Font.BOLD, 20));
		
		passwordLabel = new JLabel(Constants.passwordString);
		passwordLabel.setFont(FontLibrary.getFont("resources/fonts/typewriter.ttf", Font.BOLD, 20));
		
		registerPromptLabel = new JLabel(Constants.registerPromptString);
		registerPromptLabel.setFont(FontLibrary.getFont("resources/fonts/typewriter.ttf", Font.PLAIN, 12));
		
		usernameTF = new JTextField(5);
		usernameTF.setFont(FontLibrary.getFont("resources/fonts/typewriter.ttf", Font.BOLD, 20));
		usernameTF.setOpaque(false);
		passwordTF = new JPasswordField(5);
		passwordTF.setFont(FontLibrary.getFont("resources/fonts/typewriter.ttf", Font.BOLD, 20));
		passwordTF.setOpaque(false);
		
		loginButton = new PaintedButton(Constants.loginString,
				FontLibrary.getFont("resources/fonts/typewriter.ttf", Font.BOLD, 14),
				ImageLibrary.getImage("resources/images/buttons/redButton.png")
				);
		registerButton = new PaintedButton(Constants.registerString,
				FontLibrary.getFont("resources/fonts/typewriter.ttf", Font.BOLD, 14),
				ImageLibrary.getImage("resources/images/buttons/redButton.png")
				);
		cancelButton = new PaintedButton(Constants.cancelString,
				FontLibrary.getFont("resources/fonts/typewriter.ttf", Font.BOLD, 14),
				ImageLibrary.getImage("resources/images/buttons/redButton.png")
				);
		
		loginButton.setEnabled(false);
		
		errorLabel = new JLabel("");
		errorLabel.setFont(FontLibrary.getFont("resources/fonts/typewriter.ttf", Font.BOLD, 20));
		errorLabel.setForeground(Color.RED);
	}
	
	private void createGUI() {
		mainPanel.setLayout(new GridBagLayout());
		
		GridBagConstraints gbc = new GridBagConstraints();
		
		mainPanel.add(usernameLabel, gbc);
		gbc.gridx = 1;
		gbc.ipadx = 150;
		mainPanel.add(usernameTF, gbc);
		gbc.ipadx = 0;
		gbc.gridy = 1;
		gbc.gridx = 0;
		mainPanel.add(passwordLabel, gbc);
		gbc.gridx = 1;
		gbc.ipadx = 150;
		mainPanel.add(passwordTF, gbc);
		gbc.ipadx = 0;
		gbc.gridy = 2;
		gbc.gridx = 0;
		gbc.gridwidth = 2;
		mainPanel.add(loginButton, gbc);
		gbc.gridy = 3;
		mainPanel.add(registerPromptLabel, gbc);
		gbc.gridy = 4;
		mainPanel.add(registerButton, gbc);
		gbc.gridy = 5;
		mainPanel.add(cancelButton, gbc);
		gbc.gridy = 6;
		mainPanel.add(errorLabel, gbc);
		
		mainPanel.setOpaque(false);
		
		add(mainPanel,BorderLayout.CENTER);
	}
	
	public void clearTF(){
		usernameTF.setText("");
		passwordTF.setText("");
	}
	
	public void login(String username, String password)
	{
		client.sendObject("LOGIN:"+username+"/"+password);
	}
	
	private void addEvents() {
		loginButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				if(fromPanel == 0){
					CardLayout cl = (CardLayout)gw.setupCardPanel.getLayout();
					cl.show(gw.setupCardPanel, "start");
				}
				else if(fromPanel == 1){
					CardLayout cl = (CardLayout)gw.setupCardPanel.getLayout();
					cl.show(gw.setupCardPanel, "host");
				}
				else if(fromPanel == 2){
					CardLayout cl = (CardLayout)gw.setupCardPanel.getLayout();
					cl.show(gw.setupCardPanel, "join");
				}
				clearTF();
			}
			
		});
		loginButton.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				login(usernameTF.getText(),passwordTF.getText());
			}
		});
		registerButton.addActionListener(mRegisterAction);
		
		cancelButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				if(fromPanel == 0){
					CardLayout cl = (CardLayout)gw.setupCardPanel.getLayout();
					cl.show(gw.setupCardPanel, "start");
				}
				else if(fromPanel == 1){
					CardLayout cl = (CardLayout)gw.setupCardPanel.getLayout();
					cl.show(gw.setupCardPanel, "host");
				}
				else if(fromPanel == 2){
					CardLayout cl = (CardLayout)gw.setupCardPanel.getLayout();
					cl.show(gw.setupCardPanel, "join");
				}
				clearTF();
			}
			
		});
		
		usernameTF.getDocument().addDocumentListener(new DocumentListener() {
			public void changedUpdate(DocumentEvent e) {
				changed();
			}
			public void removeUpdate(DocumentEvent e) {
				changed();
			}
			public void insertUpdate(DocumentEvent e) {
				changed();
			}

			public void changed() {
				if (usernameTF.getText().equals("") || passwordTF.getText().equals("")){
					loginButton.setEnabled(false);
				}
				else {
					loginButton.setEnabled(true);
				}
			}
		});
		
		passwordTF.getDocument().addDocumentListener(new DocumentListener() {
			public void changedUpdate(DocumentEvent e) {
				changed();
			}
			public void removeUpdate(DocumentEvent e) {
				changed();
			}
			public void insertUpdate(DocumentEvent e) {
				changed();
			}

			public void changed() {
				if (usernameTF.getText().equals("") || passwordTF.getText().equals("")){
					loginButton.setEnabled(false);
				}
				else {
					loginButton.setEnabled(true);
				}
			}
		});
	}
	
	public void failedLogin(){
		clearTF();
		errorLabel.setText("Login Unsuccessful");
	}
	
	public void successfulLogin(Player inPlayer){
		gw.setLoggedInPlayer(inPlayer);
		gw.setLogIn(true);
		JButton fakeButton = new JButton();
		fakeButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				if(fromPanel == 0){
					CardLayout cl = (CardLayout)gw.setupCardPanel.getLayout();
					cl.show(gw.setupCardPanel, "start");
				}
				else if(fromPanel == 1){
					CardLayout cl = (CardLayout)gw.setupCardPanel.getLayout();
					cl.show(gw.setupCardPanel, "host");
				}
				else if(fromPanel == 2){
					CardLayout cl = (CardLayout)gw.setupCardPanel.getLayout();
					cl.show(gw.setupCardPanel, "join");
				}
				clearTF();
			}
			
		});
		fakeButton.doClick();
	}
	
}
