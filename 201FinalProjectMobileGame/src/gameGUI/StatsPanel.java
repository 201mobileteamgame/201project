package gameGUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

import customUI.ClearButton;
import customUI.PaintedPanel;
import gameGUI.GameWindow.SwitchPageAction;
import library.FontLibrary;

public class StatsPanel extends PaintedPanel {
private static final long serialVersionUID = 1L;
	
	private JPanel mainPanel;
	private JPanel winTeamJp;
	private JLabel winTeamLabel;
	private JPanel playerWinJp;
	private JLabel playerWinLabel;
	private JPanel winPanel;
	
	private JPanel roundsJp;
	private JLabel roundsLabel;
	private JLabel numRoundsLabel;
	
	private JLabel spyListLabel;
	private JPanel spyListJp;
	
	private ClearButton okButton;
	
	private SwitchPageAction mThankYouAction;
	private GameWindow gw;
	
	private String winningTeam = null;

	public StatsPanel(SwitchPageAction spa, GameWindow gw) {
		this.gw = gw;
		mThankYouAction = spa;
		initializeComponents();
		createGUI();
		addEvents();
	}

	private void initializeComponents() {
		mainPanel = new JPanel();
		
		winTeamJp = new JPanel();
		winTeamLabel = new JLabel();

		playerWinJp = new JPanel();
		playerWinLabel = new JLabel();
		
		winPanel = new JPanel();
		
		roundsJp = new JPanel();
		roundsLabel = new JLabel();
		numRoundsLabel = new JLabel();
		
		spyListLabel = new JLabel("Spies:");
		spyListJp = new JPanel();
		
		okButton = new ClearButton("OK",
				Constants.thankyouFont,
				Color.RED);
		
		add(mainPanel,BorderLayout.CENTER);
	}

	private void createGUI() {
		mainPanel.setLayout(new GridLayout(5,1));
		mainPanel.setOpaque(false);
		
		winPanel.setLayout(new GridLayout(2,1));
		
		winTeamLabel.setFont(FontLibrary.getFont("resources/fonts/stencil.ttf", Font.PLAIN, 40));
		winTeamJp.setLayout(new GridBagLayout());
		winTeamJp.add(winTeamLabel);
		winTeamJp.setOpaque(false);
		
		playerWinLabel.setFont(FontLibrary.getFont("resources/fonts/stencil.ttf", Font.PLAIN, 40));
		playerWinJp.setLayout(new GridBagLayout());
		playerWinJp.add(playerWinLabel);
		playerWinJp.setOpaque(false);
		
		winPanel.add(winTeamJp);
		winPanel.add(playerWinJp);
		winPanel.setOpaque(false);
		
		roundsJp.setLayout(new GridLayout(1,2));
		roundsJp.setOpaque(false);
		
		roundsLabel.setText("#Rounds: ");
		roundsLabel.setFont(Constants.typeFont);
		roundsLabel.setForeground(Color.RED);
		
		numRoundsLabel.setFont(Constants.typeFont);
		numRoundsLabel.setForeground(Color.BLACK);
		
		roundsJp.add(roundsLabel);
		roundsJp.add(numRoundsLabel);
		
		spyListLabel.setFont(Constants.typeFont);
		
		spyListJp.setLayout(new BoxLayout(spyListJp, BoxLayout.Y_AXIS));
		spyListJp.setOpaque(false);

		mainPanel.add(winPanel);
		mainPanel.add(roundsJp);
		mainPanel.add(spyListLabel);
		mainPanel.add(spyListJp);
		mainPanel.add(okButton);
		
		add(mainPanel, BorderLayout.CENTER);
	}
	
	private void addEvents() {
		okButton.addActionListener(mThankYouAction);
		okButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				gw.getClient().sendMessage("ConfirmQuit");
			}
		});
	}
	
	@Override
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		g.setXORMode(Color.LIGHT_GRAY);
		g.drawImage(Constants.spyImage, getWidth()/13*7, getHeight()/2, getWidth()/4, getHeight()/4, null);
	}
	
	public void setWinningTeam(String team) {
		winningTeam = team;
		winTeamLabel.setText(team+" Won!");
		if(team.equals("Patriots")) winTeamLabel.setForeground(Color.BLUE);
		else winTeamLabel.setForeground(Color.RED);
	}
	
	public void setPlayerWin(String role) {
		System.out.println(winTeamLabel.getText());
		if(role.equals(winningTeam)) {
			playerWinLabel.setForeground(Color.ORANGE);
			playerWinLabel.setText("You WON!");
		}
		else {
			System.out.println("Reached set you lost");
			playerWinLabel.setForeground(Color.BLACK);
			playerWinLabel.setText("You lost...");
		}
	}
	
	public void setNumRounds(int num) {
		numRoundsLabel.setText(""+num);
	}
	
	public void addSpies(String spy) {
		JLabel spyLabel = new JLabel(spy);
		spyLabel.setFont(Constants.typeFont);
		spyListJp.add(spyLabel);
		
	}
	
}
