package gameGUI;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import customUI.PaintedButton;
import customUI.PaintedPanel;
import gameGUI.GameWindow.SwitchPageAction;
import library.FontLibrary;
import library.ImageLibrary;

public class InstructionsPanel extends PaintedPanel {

	private static final long serialVersionUID = 1L;
	
	private JTextArea instructionsTA;
	private JScrollPane instructionsSP;
	private PaintedButton backButton;
	
	private JPanel topEmptyPanel;
	private JPanel bottomEmptyPanel;
	private JPanel westEmptyPanel;
	private JPanel eastEmptyPanel;
	
	private SwitchPageAction backAction;
	
	private GameWindow gw;
	
	private int fromPanel = 0;
	
	public void setFromPanel(int panel){
		fromPanel = panel;
	}
	
	InstructionsPanel(SwitchPageAction inBackAction, GameWindow inWindow) {
		backAction = inBackAction;
		gw = inWindow;
		initializeComponents();
		createGUI();
		addEvents();
	}
	
	private void initializeComponents(){
		instructionsTA = new JTextArea();
		instructionsTA.setBackground(new Color(250, 200, 100));
		instructionsTA.setEditable(false);
		instructionsTA.setLineWrap(true);
		instructionsTA.setWrapStyleWord(true);
		instructionsTA.setTabSize(4);
		instructionsTA.setFont(FontLibrary.getFont("resources/fonts/typewriter.ttf", Font.BOLD, 16));
		
		try {
			Scanner sc = new Scanner(new File("src/gameGUI/instructions"));
			while (sc.hasNext()) {
				instructionsTA.append(sc.nextLine()+"\n");
			}
			sc.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		instructionsTA.setCaretPosition(0);
		
		backButton = new PaintedButton("Back",
				FontLibrary.getFont("resources/fonts/typewriter.ttf", Font.BOLD, 14),
				ImageLibrary.getImage("resources/images/buttons/redButton.png")
				);
		
		topEmptyPanel = new JPanel();
		bottomEmptyPanel = new JPanel();
		westEmptyPanel = new JPanel();
		eastEmptyPanel = new JPanel();
		
		instructionsSP = new JScrollPane(instructionsTA);
		instructionsSP.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		instructionsSP.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
	}
	
	private void createGUI(){
		topEmptyPanel.setPreferredSize(new Dimension(520,110));
		topEmptyPanel.setOpaque(false);
		
		bottomEmptyPanel.setPreferredSize(new Dimension(520,130));
		bottomEmptyPanel.setOpaque(false);
		
		westEmptyPanel.setPreferredSize(new Dimension(80,920));
		westEmptyPanel.setOpaque(false);
		
		eastEmptyPanel.setPreferredSize(new Dimension(80,920));
		eastEmptyPanel.setOpaque(false);
		
		bottomEmptyPanel.add(backButton);
		
		add(topEmptyPanel,BorderLayout.NORTH);
		add(bottomEmptyPanel,BorderLayout.SOUTH);
		add(westEmptyPanel,BorderLayout.WEST);
		add(eastEmptyPanel,BorderLayout.EAST);
		add(instructionsSP,BorderLayout.CENTER);
	}
	
	private void addEvents(){
		backButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				if(fromPanel == 0){
					CardLayout cl = (CardLayout)gw.setupCardPanel.getLayout();
					cl.show(gw.setupCardPanel, "start");
				}
				else if(fromPanel == 1){
					CardLayout cl = (CardLayout)gw.setupCardPanel.getLayout();
					cl.show(gw.setupCardPanel, "host");
				}
				else if(fromPanel == 2){
					CardLayout cl = (CardLayout)gw.setupCardPanel.getLayout();
					cl.show(gw.setupCardPanel, "join");
				}
			}
			
		});
	}
	
}
