package gameGUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagLayout;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

import customUI.PaintedPanel;
import library.FontLibrary;

public class ThankYouPanel extends PaintedPanel {
private static final long serialVersionUID = 1L;
	
	private JPanel mainPanel;
	private JPanel doubleJp;
	private JLabel doubleLabel;
	private JPanel agentsJp;
	private JPanel daPanel;
	private JLabel agentsLabel;
	private JPanel thankyouJp;
	private JLabel thankyouLabel;
	private JPanel forPlayingJp;
	private JLabel forPlayingLabel;
	private JPanel tyfpJp;
	private JPanel waitingPanel;
	private JLabel waitingLabel;

	public ThankYouPanel() {
		initializeComponents();
		createGUI();
	}

	private void initializeComponents() {
		mainPanel = new JPanel();
		
		doubleJp = new JPanel();
		doubleLabel = new JLabel(Constants.titleString1);

		agentsJp = new JPanel();
		agentsLabel = new JLabel(Constants.titleString2);
		
		daPanel = new JPanel();
		
		thankyouJp = new JPanel();
		thankyouLabel = new JLabel();
		
		forPlayingJp = new JPanel();
		forPlayingLabel = new JLabel();
		
		tyfpJp = new JPanel();
		
		waitingPanel = new JPanel();
		waitingLabel = new JLabel();
		
		add(mainPanel,BorderLayout.CENTER);
	}

	private void createGUI() {
		mainPanel.setLayout(new GridLayout(5,1));
		mainPanel.setOpaque(false);
		
		daPanel.setLayout(new GridLayout(2,1));
		
		doubleLabel.setFont(FontLibrary.getFont("resources/fonts/stencil.ttf", Font.PLAIN, 40));
		doubleLabel.setForeground(Color.RED);
		doubleJp.setLayout(new GridBagLayout());
		doubleJp.add(doubleLabel);
		doubleJp.setOpaque(false);
		
		agentsLabel.setFont(FontLibrary.getFont("resources/fonts/stencil.ttf", Font.PLAIN, 40));
		agentsLabel.setForeground(Color.RED);
		agentsJp.setLayout(new GridBagLayout());
		agentsJp.add(agentsLabel);
		agentsJp.setOpaque(false);
		
		daPanel.add(doubleJp);
		daPanel.add(agentsJp);
		daPanel.setOpaque(false);
		
		thankyouJp.setLayout(new GridBagLayout());
		thankyouJp.setOpaque(false);
		thankyouLabel.setText("Thank You");
		thankyouLabel.setFont(Constants.thankyouFont);
		thankyouLabel.setForeground(Color.RED);
		thankyouJp.add(thankyouLabel);
		
		forPlayingJp.setLayout(new GridBagLayout());
		forPlayingJp.setOpaque(false);
		forPlayingLabel.setText("For Playing");
		forPlayingLabel.setFont(Constants.thankyouFont);
		forPlayingLabel.setForeground(Color.RED);
		forPlayingJp.add(forPlayingLabel);
		
		
		tyfpJp.setLayout(new GridLayout(2,1));
		tyfpJp.setOpaque(false);
		tyfpJp.add(thankyouJp);
		tyfpJp.add(forPlayingJp);
		
		waitingPanel.setLayout(new GridBagLayout());
		waitingPanel.setOpaque(false);
		waitingLabel.setText("Others still checking stats");
		waitingLabel.setFont(Constants.waitingFont);
		waitingLabel.setForeground(Color.BLACK);
		waitingPanel.add(waitingLabel);
		

		mainPanel.add(tyfpJp);
		mainPanel.add(new JLabel());
		mainPanel.add(new JLabel());
		mainPanel.add(daPanel);
		mainPanel.add(waitingPanel);
		
		add(mainPanel, BorderLayout.CENTER);
	}
	
	@Override
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		g.setXORMode(Color.LIGHT_GRAY);
		g.drawImage(Constants.spyImage, getWidth()/3, getHeight()/3, getWidth()/3, getHeight()/3, null);
	}
	
	
}
