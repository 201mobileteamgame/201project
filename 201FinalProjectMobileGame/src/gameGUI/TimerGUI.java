package gameGUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import library.FontLibrary;

public class TimerGUI extends JPanel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Screen screen;
	JLabel timeLabel;
	Font font;
	int time = 0;
	Timer timer;
	JPanel container;
	Boolean finished = false;
	
	public TimerGUI(){
		setLayout(new GridBagLayout());
		setBackground(new Color(0,0,0,0));
		
		screen = new Screen();
		screen.setBorder(new EmptyBorder(15,15,15,15));
		screen.setLayout(new BorderLayout());
		screen.setBackground(new Color(0,0,0,0));
		
		font = Constants.miniGameButtonsFont;
		font = font.deriveFont(Font.PLAIN, 30);
		timeLabel = new JLabel();
		timeLabel.setHorizontalAlignment(SwingConstants.CENTER);
		timeLabel.setFont(font);
		timeLabel.setForeground(Color.RED);
		timeLabel.setBorder(new EmptyBorder(5,0,5,0));
		timeLabel.setMinimumSize(new Dimension(100, 50));
		timeLabel.setMaximumSize(new Dimension(100,50));
		timeLabel.setPreferredSize(new Dimension(100,50));
		
		screen.add(timeLabel);
		add(screen);
		
		timer = new Timer();
	}
	
	private class Screen extends JPanel {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		@Override
		public void paintComponent(Graphics g){
			super.paintComponent(g);
			g.drawImage(Constants.timerForeground, 0, 0, getWidth(), getHeight(), null);
		}
	}

	@Override
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		g.drawImage(Constants.timerBackground, 0, 0, getWidth(), getHeight(), null);
	}
	
	private void paintTime(int n){
		int minutes = n/60;
		int seconds = n - minutes*60;
		String text = "";
		
		text = Integer.toString(minutes);
		text += ":";
		if(seconds < 10){
			text += "0";
		}
		text += Integer.toString(seconds);
		
		timeLabel.setText(text);
	}
	
//	public void setTime(int t){
//		this.time = t;
//		
//		timer.schedule(new TimerTask(){
//			@Override
//			public void run(){
//				if(time>=0) {
//					paintTime(time);
//					time--;
//				}
//				else {
//					finished = true;
//					paintTime(0);
//				}
//			}
//		}, 0, 1000);
//	}
	
	public void setTime(int t) {
		new CountDownTimer(t);
	}
	
	class CountDownTimer extends Thread {
		private long startTime;
		private long currentTime;
		private int timePassed = -1;
		private int time;
		
		public CountDownTimer(int time) {
			startTime = System.currentTimeMillis();
			currentTime = startTime;
			this.time = time;
			this.start();
		}
		
		public void run() {
			while(!timesUp()) {
				currentTime = System.currentTimeMillis();
				timePassed = (int) (currentTime-startTime)/1000;
				if(getTimeLeft()>=0) {
					paintTime(getTimeLeft());
				}
				try {
					Thread.sleep(200);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			timeLabel.setText("");
		}
		
		public boolean timesUp() {
			return timePassed>=time;
		}
		
		public int getTimeLeft() {
			return time-timePassed;
		}
	}
	
	public int getTime(){
		return time;
	}
	
	public boolean hasFinished(){
		return finished;
	}
}
