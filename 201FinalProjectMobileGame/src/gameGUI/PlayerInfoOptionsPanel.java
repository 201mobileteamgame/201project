package gameGUI;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JPanel;

import customUI.PaintedButton;
import gameGUI.GameWindow.SwitchPageAction;
import library.FontLibrary;
import library.ImageLibrary;

public class PlayerInfoOptionsPanel extends JPanel {
	private static final long serialVersionUID = -8653394275556967631L;
	
	private JButton recordsButton;
	private JButton loginButton;

	private SwitchPageAction mRecordsAction, mLoginAction;

	public PlayerInfoOptionsPanel(SwitchPageAction inRecordsAction, SwitchPageAction inLoginAction) {	
		setPreferredSize(Constants.topEmptyPanelDimension);
		setLayout(new GridLayout(3,5));
		recordsButton = new PaintedButton("Rules",
				FontLibrary.getFont("resources/fonts/typewriter.ttf", Font.BOLD, 18),
				ImageLibrary.getImage("resources/images/buttons/redButton.png")
				);
		loginButton = new PaintedButton("Login",
				FontLibrary.getFont("resources/fonts/typewriter.ttf", Font.BOLD, 18),
				ImageLibrary.getImage("resources/images/buttons/redButton.png")
				);
		
		mRecordsAction = inRecordsAction;
		mLoginAction = inLoginAction;
		
		add(recordsButton);
		add(Box.createGlue());
		add(Box.createGlue());
		add(Box.createGlue());
		add(loginButton);
		for(int i=0;i<10;i++) {
			add(Box.createGlue());
		}
		
		addEvents();
	}
	
	private void addEvents() {
		recordsButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println("records pressed");
			}
		});
		recordsButton.addActionListener(mRecordsAction);
		
		loginButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println("login pressed");
			}
		});
		loginButton.addActionListener(mLoginAction);
	}
	
	public void disableRecordsButton() {
		recordsButton.setEnabled(false);
	}
	
	public void disableLoginButton() {
		loginButton.setEnabled(false);
		loginButton.setForeground(Color.LIGHT_GRAY);
	}
}
