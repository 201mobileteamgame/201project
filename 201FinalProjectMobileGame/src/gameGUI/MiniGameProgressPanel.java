package gameGUI;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import javax.swing.JPanel;

public class MiniGameProgressPanel extends JPanel implements Runnable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7361313969053725520L;
	String title;
	int total;
	double completed;
	
	public MiniGameProgressPanel(){
		this.setPreferredSize(Constants.bottomEmptyPanelDimension);
		this.setOpaque(false);
		title = Constants.progressBarLabel;
		new Thread(this).start();
	}
	
	@Override
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		
		
		int w = this.getWidth();
		int h = this.getHeight();
		
		Font font = new Font("Times New Roman", Font.BOLD|Font.ITALIC, w/33);
		g.setFont(font);
		
		int strwidth = g.getFontMetrics(font).stringWidth(title);
		g.setColor(Color.WHITE);
		g.drawString(title,  (w-strwidth)/2, g.getFontMetrics(font).getHeight());
		g.setColor(Color.BLACK);
		
		final int border = 20;
		int frameX = border;
		int frameY = border+g.getFontMetrics(font).getHeight();
		int frameW = w-border-border;
		int frameH = h-border-border-g.getFontMetrics(font).getHeight();
		
		g.drawRect(frameX-1, frameY-1, frameW+1, frameH+1);
		
		int completedWidth = (int) ((completed/total)*frameW);
		int nostatusWidth = frameW-(completedWidth);
		
		g.setColor(Color.RED);
		g.fillRect(frameX, frameY, completedWidth, frameH);
		g.setColor(Color.BLACK);
		g.fillRect(frameX+completedWidth, frameY, nostatusWidth, frameH);
	}
	
	@Override
	public void run(){
		while(true){
			try{
				Thread.sleep(33);
				repaint();
			} catch(InterruptedException e){
				e.printStackTrace();
			}
		}
	}
	
	public void updateCompleted(){
		completed++;
	}
	
	public void setTotal(int total){
		this.total = total;
	}
	
	public void resetProgressBar(){
		completed = 0;
		total = 0;
	}
}
