package gameGUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import client.Client;
import customUI.PaintedButton;
import customUI.PaintedPanel;
import gameGUI.GameWindow.SwitchPageAction;
import library.FontLibrary;
import library.ImageLibrary;
import server.Server;

public class HostPanel extends PaintedPanel {
private static final long serialVersionUID = 1L;
	
	private JPanel portPanel;
	
	private PaintedButton enterButton;

	private String enterPortString = "Please Enter Port Number:";
	private JLabel enterPortLabel;
	private JTextField portTF;
	private JPanel errorPanel;
	private JLabel portErrorLabel;
	
	private ServerSocket ss;
	private int portNumber = -1;
	private Server server;
	
	private GameWindow gw;
	
	private SwitchPageAction mWaitAction;
	
	private SwitchPageAction mRecordsAction, mLoginAction;
	
	public HostPanel(SwitchPageAction spa, GameWindow gw, SwitchPageAction inRecordsAction, SwitchPageAction inLoginAction) {
		mWaitAction = spa;
		this.gw = gw;
		mRecordsAction = inRecordsAction;
		mLoginAction = inLoginAction;
		initializeComponents();
		createGUI();
		addEvents();
	}

	private void initializeComponents() {
		topPanel = new PlayerInfoOptionsPanel(mRecordsAction, mLoginAction);
		topPanel.setOpaque(false);
		
		portPanel = new JPanel();
		
		enterPortLabel = new JLabel(enterPortString);
		enterPortLabel.setFont(FontLibrary.getFont("resources/fonts/digital.ttf", Font.BOLD, 26));
		
		portTF = new JTextField();
		portTF.setFont(FontLibrary.getFont("resources/fonts/digital.ttf", Font.BOLD, 60));
		portTF.setHorizontalAlignment(JTextField.CENTER);
		
		enterButton = new PaintedButton("Enter",
				FontLibrary.getFont("resources/fonts/digital.ttf", Font.BOLD, 50),
				ImageLibrary.getImage("resources/images/buttons/redButton.png")
				);
		
		errorPanel = new JPanel();
		portErrorLabel = new JLabel();
	}

	private void createGUI() {
		add(topPanel, BorderLayout.NORTH);
		
		portPanel.setLayout(new GridLayout(7,1));
		portPanel.setOpaque(false);
		
		errorPanel.setOpaque(false);
		errorPanel.setLayout(new GridLayout(1,1));
		errorPanel.add(portErrorLabel);
		
		portErrorLabel.setOpaque(false);
		portErrorLabel.setHorizontalAlignment(JLabel.CENTER);
		portErrorLabel.setFont(FontLibrary.getFont("resources/fonts/digital.ttf", Font.BOLD, 30));
		portErrorLabel.setForeground(Color.RED);
		
		portPanel.add(Box.createGlue());
		portPanel.add(enterPortLabel);
		portPanel.add(Box.createGlue());
		portPanel.add(portTF);
		portPanel.add(Box.createGlue());
		portPanel.add(enterButton);
		portPanel.add(errorPanel);

		add(portPanel,BorderLayout.CENTER);
	}
	
	
	private void addEvents() {
		enterButton.addActionListener(new PortListener());
	}
	
	
	class PortListener implements ActionListener {
		public void actionPerformed(ActionEvent ae) {
			String portString = portTF.getText();
			portNumber = -1;
			try {
				portNumber = Integer.parseInt(portString);
			}catch (Exception e) {
				portErrorLabel.setText(e.getMessage());
				System.out.println("e in HostPanel.PortListener.actionPerformed().catch: "+e.getMessage());
				return;
			}
			if (portNumber > 0 && portNumber < 65535) {
				try {
					ServerSocket tempss = new ServerSocket(portNumber);
					ss = tempss;
					server = new Server(ss);
					Socket s = new Socket("localhost",portNumber);
					Client client = new Client(s,gw);
					if(gw.isLoggedIn()) {
						client.setPlayer(gw.getLoggedInPlayer());
					}
					gw.setHost(true);
					mWaitAction.actionPerformed(ae);
					return;
				} catch (IOException ioe) {
					// this will get thrown if I can't bind to portNumber
					portErrorLabel.setText(ioe.getMessage());
					System.out.println("ioe in HostPanel.checkValidPort(): "+ioe.getMessage());
					return;
				}
			}
			else {
				portErrorLabel.setText("Invalid port");
				return;
			}
		}
	}
	
}
