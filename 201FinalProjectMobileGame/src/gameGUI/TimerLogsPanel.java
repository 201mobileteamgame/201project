package gameGUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagLayout;
import java.awt.GridLayout;

import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import chat.ChatPanel;

public class TimerLogsPanel extends JPanel{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private ChatPanel chatPanel;
	private GameScoreGUI gameScore;
	public GameScoreGUI getGameScore() {return gameScore;}


	public TimerLogsPanel(GameWindow gw) {
	//	chatPanel = new ChatPanel(gw);
		setLayout(new FlowLayout());
		setBorder(new EmptyBorder(10,10,10,10));
		setPreferredSize(Constants.topEmptyPanelDimension);
		
		gameScore = new GameScoreGUI();
		
		add(new ClockPanel());
		add(new JLabel());
		add(gameScore);
	//	add(chatPanel);
	}
	

	class ClockPanel extends JPanel {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		private JLabel timeLabel;
		private Screen screen;
		
		public ClockPanel() {
			setLayout(new GridBagLayout());
			setOpaque(false);
			
			screen = new Screen();
			screen.setBorder(new EmptyBorder(15,20,15,20));
			screen.setLayout(new BorderLayout());
			screen.setBackground(new Color(0,0,0,0));
			
			Font font = Constants.miniGameButtonsFont;
			font = font.deriveFont(Font.PLAIN, 15);
			timeLabel = new JLabel();
			timeLabel.setFont(font);
			
			timeLabel.setForeground(Color.GREEN);
			timeLabel.setHorizontalAlignment(SwingConstants.CENTER);
			timeLabel.setBorder(new EmptyBorder(5,2,5,2));
			timeLabel.setMinimumSize(new Dimension(100, 50));
			timeLabel.setMaximumSize(new Dimension(100,50));
			timeLabel.setPreferredSize(new Dimension(100,50));
			screen.add(timeLabel);
			add(screen);
			new ClockTime();
		}
		
		private class Screen extends JPanel {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void paintComponent(Graphics g){
				super.paintComponent(g);
				g.drawImage(Constants.clockImage, 0, 0, getWidth(), getHeight(), null);
			}
		}
		
		@Override
		public void paintComponent(Graphics g){
			super.paintComponent(g);
			g.drawImage(Constants.clockImage, 0, 0, getWidth(), getHeight(), null);
		}
		
		private class ClockTime extends Thread {
			private long currentTime = -1;
			
			public ClockTime() {
				this.start();
			}
			
			public void run() {
				while(true) {
					currentTime = System.currentTimeMillis();
					timeLabel.setText(parseTime(currentTime));
					try {
						Thread.sleep(300);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}

			private String parseTime(long time) {
				int hour = (int) (time / (1000 * 60 * 60)) % 24;
				int minute = (int) (time / (1000 * 60)) % 60;	
				int second = (int) (time / 1000) % 60;
					
				//Set LA Time
				hour+=16;
				hour = hour%24;
				
				String strHour = ""+hour;
				String strMin = ""+minute;
				String strSec = ""+second;
				
				if(minute<10) {
					if(minute<=0) {
						strMin = "00";
					} else {
						strMin = "0"+minute;
					}
				}
				if(second<10) {
					if(second<=0) {
						strSec = "00";
					} else {
						strSec = "0"+second;
					}
				}
						
				return strHour+":"+strMin+":"+strSec;
			}
		}
	}
		
}
