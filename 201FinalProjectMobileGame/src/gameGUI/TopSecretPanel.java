package gameGUI;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagLayout;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

import customUI.ClearButton;
import gameGUI.GameWindow.SwitchPageAction;
import library.FontLibrary;
import library.ImageLibrary;

public class TopSecretPanel extends JPanel {
	private static final long serialVersionUID = 1L;
	
	private JPanel secretPanel;
	private ClearButton secretButton;
	private SwitchPageAction mRoleAction;

	public TopSecretPanel(SwitchPageAction spa) {
		setPreferredSize(Constants.bottomEmptyPanelDimension);
		setLayout(new GridLayout(2,1));
		mRoleAction = spa;
		
		secretPanel = new JPanel();
		secretPanel.setOpaque(false);
		secretPanel.setLayout(new GridBagLayout());
		
		secretButton = new ClearButton("TOP SECRET",
				FontLibrary.getFont("resources/fonts/stencil.ttf", Font.PLAIN, 45),
				Color.RED);
		secretButton.addActionListener(mRoleAction);
		
		secretPanel.add(secretButton);
		
		add(new JLabel());
		add(secretPanel);
		
	}
	
	@Override
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		g.drawImage(ImageLibrary.getImage("resources/images/folderTop.png"), getWidth()/10, getHeight()/5*2, getWidth()/5*4, getHeight()/5*3, null);
	}
	
}
