package gameGUI;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

import client.Client;
import customUI.PaintedPanel;
import customUI.ResizeIcon;
import database.DatabaseClient;
import game.GameManager;
import game.Player;
import library.ImageLibrary;
import score.ScoreTable;
import sound.GameAudio;

public class GameWindow extends JFrame {
	
	private static final long serialVersionUID = 1L;
	
	OptionsWindow optionsWindow;
	
	JPanel setupCardPanel;
	private StartPanel startPanel;
	private LoginPanel loginPanel;
	public LoginPanel getLoginPanel(){return loginPanel;}

	private RegisterUserPanel registerUserPanel;
	private ScoreTable scoreTable;
	private WaitPanel waitPanel;
	public WaitPanel getWaitPanel() {return waitPanel;}
	private HostPanel hostPanel;
	private JoinPanel joinPanel;
	private InstructionsPanel instructionsPanel;
	public InstructionsPanel getInstructionsPanel(){return instructionsPanel;}
	private LeaderSelectPanel leaderSelectPanel;
	public LeaderSelectPanel getLeaderSelectPanel() {return leaderSelectPanel;}
	private TeamPanel teamPanel;
	public TeamPanel getTeamPanel() {return teamPanel;}
	private RolePanel rolePanel;
	public RolePanel getRolePanel() {return rolePanel;}
	private MiniGame1 miniGame1;
	private MiniGame2 miniGame2;
	private MiniGame3 miniGame3;
	private MiniGame4 miniGame4;
	private SpectatorPanel spectator;
	public SpectatorPanel getSpectator() {return spectator;}
	private StatsPanel statsPanel;
	public StatsPanel getStatsPanel() {return statsPanel;}
	private ThankYouPanel thankyouPanel;
	private DatabaseClient dbClient;
	
	private MiniGameProgressPanel progressBar;
	
	private GameManager gameManager;
	public GameManager getGameManager() {return gameManager;}
	private Client client = null;
	private boolean isHost = false;
	private boolean isTraitor = false;
	
	private boolean loggedIn = false;
	private Player loggedInPlayer = null;
	
	PaintedPanel gameplayBackgroundPanel;

	public GameWindow() {
		super("Double Agents");
		//sets main window dimensions
		setSize(540,960);
		setLocation(100,50);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setMinimumSize(new Dimension(360,640));
		this.setMaximumSize(new Dimension(540,960));
		
		setIconImage(ImageLibrary.getImage("resources/images/spy.png"));		
		
		//Create all the necessary panels in a card layout and add action listeners
		refreshSetUpComponents();
		
		optionsWindow = new OptionsWindow(this);
		
		
		//Options menu
		JPanel optionsPanel = new JPanel();
		optionsPanel.setLayout(new BoxLayout(optionsPanel, BoxLayout.X_AXIS));
		optionsPanel.setPreferredSize(new Dimension(this.getWidth(), 30));
		
		JMenuBar jmb = new JMenuBar();
		jmb.setOpaque(false);
		JMenuItem optionsMenu = new JMenuItem();
		optionsMenu.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		optionsMenu.setIcon(ResizeIcon.rs(new ImageIcon("resources/images/options.png"), 20, 20));
		
		optionsMenu.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if(optionsWindow.isVisible()) optionsWindow.setVisible(false);
				else optionsWindow.appear();
			}
		});
		
		jmb.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		jmb.add(optionsMenu);
		jmb.add(Box.createHorizontalGlue());
		optionsPanel.add(jmb);
		add(optionsPanel, BorderLayout.SOUTH);
		
		//Window Movement stuff
		addComponentListener(new ComponentAdapter() {
	         public void componentMoved(ComponentEvent e) {
	            optionsWindow.setVisible(false);
	         }
	      });
		
		GameAudio ga = new GameAudio("resources/audio/007.wav");
	}
	
	public void setLogIn(boolean logIn) {
		loggedIn = logIn;
	}
	
	
	public boolean isLoggedIn() {
		return loggedIn;
	}
	
	public void setDatabaseClient(DatabaseClient dc)
	{
		this.dbClient = dc;
	}
	
	public DatabaseClient getDatabaseClient()
	{
		return this.dbClient;
	}
	
	public void setClient(Client c) {
		client = c;
	}
	
	public Client getClient() {
		if(client==null) return null;
		return client;
	}
	
	public void setHost(boolean host) {
		isHost = host;
	}
	
	public boolean getIsHost() {
		return isHost;
	}
	
	public void setTotalNumTasks(int num) {
		miniGame1.setTotalMission(num);
		miniGame2.setTotalMission(num);
		miniGame3.setTotalMission(num);
		miniGame4.setTotalMission(num);
	}
	
	public void startAllMission() {
		miniGame1.startGame();
		miniGame2.startGame();
		miniGame3.startGame();
		miniGame4.startGame();
		spectator.startGame();
	}
	
	public void quitAllMissions() {
		miniGame1.leaveGame();
		miniGame2.leaveGame();
		miniGame3.leaveGame();
		miniGame4.leaveGame();
		spectator.leaveGame();
	}
	
	public void updateTasksCompleted(){
		miniGame1.updateTasks();
		miniGame2.updateTasks();
		miniGame3.updateTasks();
		miniGame4.updateTasks();
	}
	
	public void callGameEnd() {
		//TODO
		teamPanel.enterFinalPanel();
	}
	
	public void refreshSetUpComponents() {		
		//center panel that holds the cards to switch between panels
		setupCardPanel = new JPanel();
		setupCardPanel.setLayout(new CardLayout());
		
		//adding cards to the center panel
		startPanel = new StartPanel(new SwitchPageAction(setupCardPanel, "host"), new SwitchPageAction(setupCardPanel, "join"), new SwitchPageAction(setupCardPanel, "instructions"), new SwitchPageAction(setupCardPanel, "login"), this);
		setupCardPanel.add(startPanel, "start");
		
		hostPanel = new HostPanel(new SwitchPageAction(setupCardPanel, "wait"), this, new SwitchPageAction(setupCardPanel, "instructions"), new SwitchPageAction(setupCardPanel, "login"));
		setupCardPanel.add(hostPanel, "host");
		
		instructionsPanel = new InstructionsPanel(new SwitchPageAction(setupCardPanel, "start"), this);
		setupCardPanel.add(instructionsPanel, "instructions");
		
		joinPanel = new JoinPanel(new SwitchPageAction(setupCardPanel, "wait"), this, new SwitchPageAction(setupCardPanel, "instructions"), new SwitchPageAction(setupCardPanel, "login"));
		setupCardPanel.add(joinPanel, "join");
		
		loginPanel = new LoginPanel(new SwitchPageAction(setupCardPanel, "start"), new SwitchPageAction(setupCardPanel, "register"), dbClient,this);
		setupCardPanel.add(loginPanel, "login");
		
		registerUserPanel = new RegisterUserPanel(new SwitchPageAction(setupCardPanel, "login"),dbClient,this);
		setupCardPanel.add(registerUserPanel, "register");
	
		scoreTable = new ScoreTable(new SwitchPageAction(setupCardPanel, "start"));
		setupCardPanel.add(scoreTable, "score");
		
		waitPanel = new WaitPanel(new SwitchPageAction(setupCardPanel, "team"), this, new SwitchPageAction(setupCardPanel, "score"));
		setupCardPanel.add(waitPanel, "wait");
		
		leaderSelectPanel = new LeaderSelectPanel(new SwitchPageAction(setupCardPanel, "team"), this);
		setupCardPanel.add(leaderSelectPanel, "leader");
		
		teamPanel = new TeamPanel(new SwitchPageAction(setupCardPanel, "miniGame1"),
				new SwitchPageAction(setupCardPanel, "miniGame2"), new SwitchPageAction(setupCardPanel, "miniGame3"),
				new SwitchPageAction(setupCardPanel, "miniGame4"), new SwitchPageAction(setupCardPanel, "leader"), 
				new SwitchPageAction(setupCardPanel, "spectator"), new SwitchPageAction(setupCardPanel, "role"), 
				new SwitchPageAction(setupCardPanel, "stats"), this);
		setupCardPanel.add(teamPanel, "team");
		
		rolePanel = new RolePanel(new SwitchPageAction(setupCardPanel, "team"), this);
		setupCardPanel.add(rolePanel, "role");
		
		miniGame1 = new MiniGame1(new SwitchPageAction(setupCardPanel, "team"), this);
		setupCardPanel.add(miniGame1, "miniGame1");
		
		miniGame2 = new MiniGame2(new SwitchPageAction(setupCardPanel, "team"), this);
		setupCardPanel.add(miniGame2, "miniGame2");
		
		miniGame3 = new MiniGame3(new SwitchPageAction(setupCardPanel, "team"), this);
		setupCardPanel.add(miniGame3, "miniGame3");
		
		miniGame4 = new MiniGame4(new SwitchPageAction(setupCardPanel, "team"), this);
		setupCardPanel.add(miniGame4, "miniGame4");
		
		spectator = new SpectatorPanel(new SwitchPageAction(setupCardPanel, "team"), this);
		setupCardPanel.add(spectator, "spectator");
		
		statsPanel = new StatsPanel(new SwitchPageAction(setupCardPanel, "thankyou"), this);
		setupCardPanel.add(statsPanel, "stats");
		
		thankyouPanel = new ThankYouPanel();
		setupCardPanel.add(thankyouPanel, "thankyou");
		
		add(setupCardPanel, BorderLayout.CENTER);
		
		gameManager = new GameManager();
	}
	
	public void setTraitor(boolean t) {
		isTraitor = t;
	}
	
	public boolean getIsTraitor() {
		return isTraitor;
	}
	
	public void sendNewTask(String player, String taskString) {
		if(client.getPlayer().getPlayerName().equals(player)) {
			System.out.println(player+" getting task! : "+taskString);
			miniGame1.setInstruction(taskString);
			miniGame2.setInstruction(taskString);
			miniGame3.setInstruction(taskString);
			miniGame4.setInstruction(taskString);
		}
	}
	
	public void freezeAllMissions() {
		miniGame1.freeze();
		miniGame2.freeze();
		miniGame3.freeze();
		miniGame4.freeze();
	}
	
	public class SwitchPageAction implements ActionListener {
		private String pageString;
		private JPanel jp;
		
		public SwitchPageAction (JPanel jp, String pageString) {
			this.pageString = pageString;
			this.jp = jp;
		}
		public void actionPerformed(ActionEvent ae) {
			CardLayout cl = (CardLayout)jp.getLayout();
			cl.show(jp, pageString);
		}
	}
	
	public void setLoggedInPlayer(Player inPlayer){
		loggedInPlayer = inPlayer;
		System.out.println("Logged in player is: "+loggedInPlayer.getPlayerName());
	}
	
	public Player getLoggedInPlayer(){
		System.out.println("Logged in player is: "+loggedInPlayer.getPlayerName());
		return loggedInPlayer;
	}

	
	public static void main(String [] args) {
		GameWindow gameWindow = new GameWindow();
		gameWindow.setVisible(true);
	}
	
}
