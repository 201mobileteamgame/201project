package gameGUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;

import customUI.ClearButton;
import customUI.PaintedPanel;
import gameGUI.GameWindow.SwitchPageAction;
import library.FontLibrary;
import library.ImageLibrary;

public class WaitPanel extends PaintedPanel {
private static final long serialVersionUID = 1L;
	
	private JPanel waitingPanel;

	private JLabel waitLabel;
	
	private JScrollPane waitSP;
	private static JTable waitTable;
	private static DefaultTableModel waitTableModel;
	private String[] columns = {"Name"};
	
	private SwitchPageAction mTeamAction;
	private SwitchPageAction mRecordsAction;
	
	private JPanel startButtonPanel;
	private ClearButton startButton;
	
	private Vector<String> toDisplay;
	
	private JPanel mainPanel;
	
	private GameWindow gw;
	
	public WaitPanel(SwitchPageAction teamAction, GameWindow gw, SwitchPageAction inRecordsAction) {
		mTeamAction = teamAction;
		this.gw = gw;
		mRecordsAction = inRecordsAction;

		initializeComponents();
		createGUI();
	}

	private void initializeComponents() {
		mainPanel = new JPanel();
		waitingPanel = new JPanel();
		waitLabel = new JLabel("Waiting for other Agents...");
		waitTableModel = new DefaultTableModel(columns, 0) {
			private static final long serialVersionUID = 1L;
			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		waitTable = new JTable(waitTableModel);
		waitSP = new JScrollPane(waitTable);
		
		startButtonPanel = new JPanel();
		startButton = new ClearButton("Start",
				FontLibrary.getFont("resources/fonts/digital.ttf", Font.BOLD, 30),
				Color.BLACK
				);
	}

	private void createGUI() {
		setOpaque(false);
		
		topPanel = new PlayerInfoOptionsPanel(mRecordsAction, mRecordsAction);
		topPanel.setOpaque(false);
		add(topPanel, BorderLayout.NORTH);
		
		((PlayerInfoOptionsPanel) topPanel).disableLoginButton();
		((PlayerInfoOptionsPanel) topPanel).disableRecordsButton();
		
		waitLabel.setFont(FontLibrary.getFont("resources/fonts/typewriter.ttf", Font.PLAIN, 25));
		waitLabel.setHorizontalAlignment(JLabel.CENTER);
		
		waitTable.setRowHeight(40);
		waitTable.setFont(FontLibrary.getFont("resources/fonts/typewriter.ttf", Font.PLAIN, 25));
		waitTable.setCellSelectionEnabled(false);
		waitTable.setGridColor(Color.BLACK);
		
		waitSP.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		
		JTableHeader patriotHeader = waitTable.getTableHeader();
		patriotHeader.setPreferredSize(new Dimension(waitSP.getWidth(),40));
		patriotHeader.setFont(FontLibrary.getFont("resources/fonts/typewriter.ttf", Font.PLAIN, 20));
		
		waitingPanel.setLayout(new BorderLayout());
		waitingPanel.setOpaque(false);
		waitingPanel.add(waitLabel,BorderLayout.NORTH);
		waitingPanel.add(waitSP,BorderLayout.CENTER);
		
		startButtonPanel.setLayout(new GridLayout(1,3));
		startButtonPanel.setPreferredSize(new Dimension(520,50));
		startButtonPanel.setOpaque(false);
		startButtonPanel.add(Box.createGlue());
		startButtonPanel.add(Box.createGlue());
		startButtonPanel.add(startButton);
		
		startButton.setEnabled(false);

		mainPanel.setLayout(new BorderLayout());
		mainPanel.setOpaque(false);
		mainPanel.add(waitingPanel,BorderLayout.CENTER);
		mainPanel.add(startButtonPanel,BorderLayout.SOUTH);
		
		add(mainPanel,BorderLayout.CENTER);
	}
	
	public void enterTeamPanel() {
		JButton jb = new JButton();
		jb.addActionListener(mTeamAction);
		jb.doClick();
	}
//	
//	public void enterTeamPanel() {
//		startButton.setEnabled(true);
//		startButton.addActionListener(mTeamAction);
//		startButton.doClick();
//		if(gw.getIsHost() && ssa!=null) {
//			startButton.removeActionListener(ssa);
//		}
//	}
	
	public void displayPlayers() {
		toDisplay = gw.getClient().getPlayerVector();
		waitTableModel.setRowCount(0);
		int count = 0;
		for(String name : toDisplay) {
			Object[] row = {name};
			waitTableModel.insertRow(count, row);
		}
		gw.getLeaderSelectPanel().setPlayerList(toDisplay);
		gw.getTeamPanel().setPlayerIdentity(gw.getClient().getPlayer().getPlayerName());
	}
	
	public void enableStart() {
		System.out.println("This client is host: "+gw.getIsHost());
		if(gw.getIsHost()) {
			startButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					gw.getClient().sendMessage("GameStart");
				}
			});
			startButton.setEnabled(true);
		}
	}
	
}
