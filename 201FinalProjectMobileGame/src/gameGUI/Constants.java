package gameGUI;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;

import javax.swing.Icon;
import javax.swing.ImageIcon;

import library.FontLibrary;
import library.ImageLibrary;

public class Constants {
	
	//GUI main window dimensions
	public static final int guiWidth = 540;
	public static final int guiHeight = 960;
	
	//Empty centering panel dimensions
	public static final Dimension topEmptyPanelDimension = new Dimension(520,110);
	public static final Dimension bottomEmptyPanelDimension = new Dimension(520,140);
	public static final Dimension westEmptyPanelDimension = new Dimension(80,920);
	public static final Dimension eastEmptyPanelDimension = new Dimension(80,920);
	
	//All fonts used during the game
	public static final Font miniGameButtonsFont = FontLibrary.getFont("resources/fonts/kenvector_future_thin.ttf", Font.PLAIN, 15);
	public static final Font miniGameScreenFont = FontLibrary.getFont("resources/fonts/kenvector_future_thin.ttf", Font.PLAIN, 33);
	public static final Font scoreFont = FontLibrary.getFont("resources/fonts/kenvector_future_thin.ttf", Font.BOLD, 16);
	public static final Font thankyouFont = FontLibrary.getFont("resources/fonts/stencil.ttf", Font.PLAIN, 40);
	public static final Font waitingFont = FontLibrary.getFont("resources/fonts/stencil.ttf", Font.PLAIN, 20);
	public static final Font typeFont = FontLibrary.getFont("resources/fonts/typewriter.ttf", Font.BOLD, 30);
	
	//All gui element labels used during the game
	public static final String titleString1 = "DOUBLE";
	public static final String titleString2 = "AGENTS";
	public static final String recordsString = "Records";
	public static final String loginString = "Login";
	public static final String hostString = "Host";
	public static final String joinString = "Join";
	public static final String teamString = "Team";
	public static final String roundsString = "Rounds";
	public static final String usernameString = "Username: ";
	public static final String passwordString = "Password: ";
	public static final String confirmString = "Confirm password: ";
	public static final String hintString = "Password Hint: ";
	public static final String registerString = "Register";
	public static final String registerPromptString = "Don't have an account?";
	public static final String waitingString = "Waiting for other agents...";
	public static final String progressBarLabel = "Mission Progress: ";
	public static final String patriotsLabel = "Patriots: ";
	public static final String traitorsLabel = "Traitors: ";
	public static final String cancelString = "Cancel";
	public static final String instructionsString = "Instructions";
	
	//All images used during the game
	public static final Image timerBackground = ImageLibrary.getImage("resources/images/timerBackground.png");
	public static final Image timerForeground = ImageLibrary.getImage("resources/images/timerForeground.png");
	public static final Image clockImage = ImageLibrary.getImage("resources/images/GUI_Final/DA_Clock.png");
	
	public static final Image miniGameBackground = ImageLibrary.getImage("resources/images/spyBG.png");
	public static final Image miniGameScreenImage = ImageLibrary.getImage("resources/images/panels/card_beige.png");
	public static final Image spyImage = ImageLibrary.getImage("resources/images/spy.png");
	public static final Image miniButtonImage = ImageLibrary.getImage("resources/images/buttons/buttonBackground.png");
	
	public static final Image scoreCheckBox = ImageLibrary.getImage("resources/images/blackBoxIcon.png");
	
	public static final Image patriotsWinImage = ImageLibrary.getImage("resources/images/torchIcon.png");
	public static final Image traitorsWinImage = ImageLibrary.getImage("resources/images/knifeIcon.png");
	
	public static final ImageIcon patriotsWinIcon = new ImageIcon("resources/images/kifeIcon.png");
	public static final ImageIcon traitorsWinIcon = new ImageIcon("resources/images/torchIcon.png");
	
	//All buttons used during the game
	public static final ImageIcon spyButtonIcon = ImageLibrary.getIcon("resources/images/BlackButton.png");
	public static final ImageIcon redButtonIcon = ImageLibrary.getIcon("resources/images/buttons/redButton.png");
	public static final ImageIcon redButtonPressedIcon = ImageLibrary.getIcon("resources/images/buttons/redButton.png");
	
	//Time used for the timerGUI
	public static final int oneMinute = 60;
	
}
