package chat;

import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;

public class PopUpChat extends JFrame{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -711527522998309720L;
	ChatPanel chatPanel;
	ChatFrame chatFrame;
	public PopUpChat(ChatPanel cp, ChatFrame cf){
		chatPanel = cp;
		chatFrame = cf;
		add(cp);
		setSize(540,960);
		setLocation(100,50);
		this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		this.setMinimumSize(new Dimension(360,640));
		this.setMaximumSize(new Dimension(540,960));
		addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
            		chatFrame.setVisible(true);
            		chatFrame.setState(Frame.NORMAL);
                    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                    setVisible(false);
                    dispose();
                }
            });
	}
}

