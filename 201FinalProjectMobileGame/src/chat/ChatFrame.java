package chat;

import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class ChatFrame extends JFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	ChatPanel chatPanel;
	PopUpChat popUp;
	ChatFrame thisFrame;
	
	public ChatFrame(ChatPanel cp){
		thisFrame = this;
		chatPanel = cp;
		add(chatPanel);
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
                    @Override
                    public void windowClosing(WindowEvent e) {
                    		popUp = new PopUpChat(chatPanel, thisFrame);
                    		popUp.add(chatPanel);
                            setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
                            setVisible(false);
                            dispose();
                        }
                    });
		}
}
