package chat;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import client.Client;
import customUI.PaintedButton;
import gameGUI.GameWindow;
import library.FontLibrary;
import library.ImageLibrary;

public class ChatPanel extends JPanel{

	private static final long serialVersionUID = 1L;
	
	private JPanel convoPanel;
	private JScrollPane convoScroll;
	
	private JPanel typePanel;
	private JTextField chatTF;
	private PaintedButton sendButton;
	
	private Client client = null;
	
	private GameWindow gw;

	public ChatPanel(GameWindow gw) {
		this.gw = gw;
		initializeComponents();
		createGUI();
		addEvents();
	}

	private void initializeComponents() {
		convoPanel = new JPanel();
		convoScroll = new JScrollPane(convoPanel);
		typePanel = new JPanel();
		chatTF = new JTextField(30);
		sendButton = new PaintedButton(
					"Send",
					FontLibrary.getFont("resources/fonts/typewriter.ttf", Font.PLAIN, 20),
					ImageLibrary.getImage("resources/images/spy.png")
				);
	}
	
	private void createGUI() {
		setLayout(new BorderLayout());
		
		convoPanel.setBackground(Color.CYAN);
		convoPanel.setFont(FontLibrary.getFont("resources/fonts/typewriter.ttf", Font.PLAIN, 12));
		convoPanel.setLayout(new BoxLayout(convoPanel,BoxLayout.Y_AXIS));
		convoScroll.setPreferredSize(new Dimension(200,60));
		convoScroll.setBackground(Color.BLACK);
		convoScroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		
		typePanel.setLayout(new BorderLayout());
		typePanel.setBackground(Color.CYAN);
		chatTF.setBackground(Color.CYAN);
		chatTF.setForeground(Color.WHITE);
		chatTF.setFont(FontLibrary.getFont("resources/fonts/typewriter.ttf", Font.PLAIN, 12));
		typePanel.add(chatTF, BorderLayout.CENTER);
		typePanel.add(sendButton, BorderLayout.EAST);
		
		add(convoScroll,BorderLayout.CENTER);
		add(typePanel, BorderLayout.SOUTH);
	}
	
	private void addEvents() {
		sendButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				if(chatTF.getText()!=null) {
					if(chatTF.getText().length()>=1) {
						String newMessage = chatTF.getText();
						client.sendMessage("Chat: "+gw.getClient().getPlayer().getPlayerName()+" "+newMessage);
						chatTF.setText("");
						revalidate();
						repaint();
					}
				}
			}
		});
		
	}
/*	
	public void addDisconnectMessage(String strColor,Color color) {
		JPanel labelPanel = new JPanel();
		labelPanel.setLayout(new BoxLayout(labelPanel,BoxLayout.X_AXIS));
		JLabel playerLabel = new JLabel(strColor+" has left the game");
		playerLabel.setFont(FontLibrary.getFont("fonts/kenvector_future_thin.ttf", Font.PLAIN, 12));
		playerLabel.setForeground(color);
		
		labelPanel.add(playerLabel);
		labelPanel.setOpaque(false);
		labelPanel.setAlignmentX(LEFT_ALIGNMENT);
		
		convoPanel.add(labelPanel);
		convoPanel.revalidate();
	    int height = (int)convoPanel.getPreferredSize().getHeight();
	    Rectangle rect = new Rectangle(0,height,10,10);
        convoPanel.scrollRectToVisible(rect);
		revalidate();
		repaint();
	}*/
	
	public void addNewMessage(String message, String playerName) {
		JPanel labelPanel = new JPanel();
		labelPanel.setLayout(new BoxLayout(labelPanel,BoxLayout.X_AXIS));
		JLabel playerLabel = new JLabel(playerName+": ");
		playerLabel.setFont(FontLibrary.getFont("resources/fonts/typewriter.ttf", Font.PLAIN, 12));
		playerLabel.setForeground(Color.LIGHT_GRAY);
		
		JLabel newMessageLabel = new JLabel(message);
		newMessageLabel.setFont(FontLibrary.getFont("resources/fonts/typewriter.ttf", Font.PLAIN, 12));
		newMessageLabel.setForeground(Color.WHITE);
		
		labelPanel.add(playerLabel);
		labelPanel.add(newMessageLabel);
		labelPanel.setOpaque(false);
		labelPanel.setAlignmentX(LEFT_ALIGNMENT);
		
		convoPanel.add(labelPanel);
		convoPanel.revalidate();
	    int height = (int)convoPanel.getPreferredSize().getHeight();
	    Rectangle rect = new Rectangle(0,height,10,10);
        convoPanel.scrollRectToVisible(rect);
		revalidate();
		repaint();
	}
	
	public void setClient(Client c) {
		client = c;
	}
	
}
