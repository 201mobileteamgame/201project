package sound;

public class GameAudioNoLoop extends Thread
{
	public String gameMusicPath;
	public AudioPlayerNoLoop gameMusic;
	public GameAudioNoLoop(String gameMusicPath)
	{
		gameMusic = new AudioPlayerNoLoop();
		this.gameMusicPath=gameMusicPath;
		start();
	}
	public void run()
	{
		gameMusic.play(gameMusicPath);
	}
}
