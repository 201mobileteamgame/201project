package sound;

public class GameAudio extends Thread
{
	public String gameMusicPath;
	public AudioPlayer gameMusic;
	public GameAudio(String gameMusicPath)
	{
		gameMusic = new AudioPlayer();
		this.gameMusicPath=gameMusicPath;
		start();
	}
	public void run()
	{
		while (true)
		{
			gameMusic.play(gameMusicPath);
			gameMusic = new AudioPlayer();
		}
	}
}
