package sound;

import java.io.File;
import java.io.IOException;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.FloatControl;
import javax.sound.sampled.LineEvent;
import javax.sound.sampled.LineListener;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

public class AudioPlayer implements LineListener {
	private boolean playCompleted = false;
	private float volumeDBOffset = 0.0f;

	public void play(String audioFilePath) {
		//Create an AudioInputStream from a given sound file
		File audioFile = new File(audioFilePath);
		
		AudioInputStream audioStream = null;
		Clip audioClip = null;		
		try {
			audioStream = AudioSystem.getAudioInputStream(audioFile);
			//Acquire audio format and create a DataLine.Info Object
			AudioFormat format = audioStream.getFormat();			
			DataLine.Info info =  new DataLine.Info(Clip.class, format);
			//Obtain the clip
			audioClip = (Clip) AudioSystem.getLine(info);			
			audioClip.addLineListener(this);
			//Open the AudioInputStream and start playing
			audioClip.open(audioStream);
			
			FloatControl gainControl = (FloatControl) audioClip.getControl(FloatControl.Type.MASTER_GAIN);
			if(volumeDBOffset <= gainControl.getMaximum() && volumeDBOffset >= gainControl.getMinimum()) {
				gainControl.setValue(volumeDBOffset);
			}
			
			
			audioClip.start();
			
			while(!playCompleted) {
				try {
					Thread.sleep(audioClip.getMicrosecondLength()/1000000);
				} catch (InterruptedException ie) {
					ie.printStackTrace();
				}
			}			
		} catch (UnsupportedAudioFileException uafe) {
			System.out.println("The specified audio file is not supported.");
			uafe.printStackTrace();
		} catch (LineUnavailableException lue) {
			System.out.println("Audio line for playing back is unavailable.");
			lue.printStackTrace();
		} catch (IOException ioe) {
			System.out.println("Error playing the audio file.");
			ioe.printStackTrace();
		}
		//Close and release resources acquired
		finally {
			if(audioClip != null) {
				audioClip.close();
			}
			if(audioStream != null) {
				try {
					audioStream.close();
				} catch (IOException ioe) {
					System.out.println("Error closing audio stream.");
					ioe.printStackTrace();
				}
			}
		}
	}
	
	public void setVolumeDBOffset(float inOffset) {
		volumeDBOffset = inOffset;
	}

	@Override
	public void update(LineEvent event) {
		LineEvent.Type type = event.getType();
		
		if(type == LineEvent.Type.STOP) {
			playCompleted = true;
		}
	}
}
