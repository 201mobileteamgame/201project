package database;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.Socket;


public class DatabaseServerThread extends Thread
{
	private Socket mSocket;
	private ObjectOutputStream oos;
	private ObjectInputStream ois;
	private DatabaseServer mServer;
	private int identifier;
	
	public DatabaseServerThread (Socket inS, DatabaseServer inServer,int identifier)
	{
		mSocket = inS;
		this.identifier=identifier;
		mServer = inServer;
		boolean socketReady = initializeVariables();
		if (socketReady) {
			System.out.println("Database Server Thread "+ identifier +" is set up");
			start();
		}
	}
	public Socket getSocket()
	{
		return mSocket;
	}
	private boolean initializeVariables() 
	{
		try 
		{
			oos = new ObjectOutputStream(mSocket.getOutputStream());
			ois = new ObjectInputStream(mSocket.getInputStream());
		} 
		catch (IOException ioe) 
		{
			Util.printExceptionToCommand(ioe);
			//Util.printMessageToCommand(Constants.unableToGetStreams);
			return false;
		}
		return true;
	}
	public void sendObject(Object obj) {
		try 
		{
			if(oos != null) 
			{
				oos.writeObject(obj);
				oos.flush();
			}
		} 
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} //Always need to flush after printing a line
	}
	public Object receiveObject()
	{
		Object obj=null;
		try {
			obj = ois.readObject();
		} catch (ClassNotFoundException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (obj==null)
		{
			System.out.println("Object is not received");
		}
		return obj;
	}
	public void run() {
		sendObject("Identifier:"+identifier);
		while(true) {
			try {
				//f (ois.)
				{
					Object obj =receiveObject();
					System.out.println("recieive something "+obj);
					String message = (String)obj;//receiveObject();
					System.out.println(message);
					if (message == null) {
						break;
					}
					if (message.split(":")[0].equals("LOGIN"))
						mServer.processLogin(identifier,message.split(":")[1]);
					else if (message.split(":")[0].equals("REGISTER"))
					{
						System.out.println("Server trying to add user:");
						mServer.registerUser(message.split(":")[1]);
						System.out.println("Users added");
				
					}
						/*else if (message.equals("CHECKCOLORS"))
						mServer.checkColors();
					else if (message.split(":")[0].equals("CHAT"))
						mServer.sendChatMessageEverywhere(identifier,message.split(":")[1]);
					else
					{
						System.out.println("Sending message to all clients "+message);
						mServer.relayMessageToAllClients(message,this);
					}*/
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}