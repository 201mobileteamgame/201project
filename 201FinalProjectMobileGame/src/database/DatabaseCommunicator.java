package database;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import game.Player;

public class DatabaseCommunicator 
{
	private MySQLDriver driver;
	public DatabaseCommunicator()
	{
		driver = new MySQLDriver();
		driver.connect();
	}
	
	public void stop()
	{
		driver.stop();
	}
	public boolean doesUserExist(String name)
	{
		return driver.doesUserExistByName(name);
	}
	public void createNewUser(String name,String pass,String hint)
	{
		driver.addUserProfile(name, (pass), hint);
	}
	public Player login(String name, String password)
	{
		if (doesUserExist(name))
		{
			String pass = driver.getUserPassFromName(name);
			if ((password).equals(pass))
			{
				Player temp = new Player(true);
				temp.setPlayerName(name);
				return temp;
			}
		}
		Player temp = null;
		return temp;
	}
	public String getPasswordHint(String name)
	{
		return driver.getUserPassHintFromName(name);
	}
	private String encryption(String password)
	{
		return ""+password.hashCode();
	}
	
	
	
	
	
	
	public void setAverageScore(String name, double avgScore)
	{
		if (driver.doesUserExistByName(name))
		{
			int id = driver.getUserIDFromName(name);
			driver.addAverageScoreToStats(id, avgScore);
		}
	}
	public void setHighScore(String name, int highScore)
	{
		if (driver.doesUserExistByName(name))
		{
			int id = driver.getUserIDFromName(name);
			driver.addHighScoreToStats(id, highScore);
		}
	}
	public void setTotalScore(String name, int totalScore)
	{
		if (driver.doesUserExistByName(name))
		{
			int id = driver.getUserIDFromName(name);
			driver.addTotalScoreToStats(id, totalScore);
		}
	}
	public void setNumberGamesPlayed(String name, int gamesPlayed)
	{
		if (driver.doesUserExistByName(name))
		{
			int id = driver.getUserIDFromName(name);
			driver.addNumberGamesPlayedToStats(id, gamesPlayed);
		}
	}
	
	public void setNumberLeaderApprovals(String name, int numberApprovals)
	{
		if (driver.doesUserExistByName(name))
		{
			int id = driver.getUserIDFromName(name);
			driver.addNumberLeaderApprovalsToStats(id, numberApprovals);
		}
	}
	
	public void setNumberLeaderRejections(String name, int numberRejections)
	{
		if (driver.doesUserExistByName(name))
		{
			int id = driver.getUserIDFromName(name);
			driver.addNumberLeaderRejectionsToStats(id, numberRejections);
		}
	}
	
	public void setNumberWins(String name, int numberWins)
	{
		if (driver.doesUserExistByName(name))
		{
			int id = driver.getUserIDFromName(name);
			driver.addNumberWinsToStats(id, numberWins);
		}
	}
	
	public void setNumberLosses(String name, int numberLosses)
	{
		if (driver.doesUserExistByName(name))
		{
			int id = driver.getUserIDFromName(name);
			driver.addNumberLossesToStats(id, numberLosses);
		}
	}
	
	public void setNumberTimesPatriot(String name, int timesPatriot)
	{
		if (driver.doesUserExistByName(name))
		{
			int id = driver.getUserIDFromName(name);
			driver.addNumberTimesPatriotToStats(id, timesPatriot);
		}
	}
	
	public void setNumberTimesTraitor(String name, int timesTraitor)
	{
		if (driver.doesUserExistByName(name))
		{
			int id = driver.getUserIDFromName(name);
			driver.addNumberTimesTraitorToStats(id, timesTraitor);
		}	
	}
	
	public void setQuickestSabotage(String name, int quickestSabotage)
	{
		if (driver.doesUserExistByName(name))
		{
			int id = driver.getUserIDFromName(name);
			driver.addQuickestSabotageToStats(id, quickestSabotage);
		}	
	}
	public void setLongestSabotage(String name, int longestSabotage)
	{
		if (driver.doesUserExistByName(name))
		{
			int id = driver.getUserIDFromName(name);
			driver.addLongestSabotageToStats(id, longestSabotage);
		}
	}
	public void setMissionAccuracyAverage(String name, double missionAccuracyAverage)
	{
		if (driver.doesUserExistByName(name))
		{
			int id = driver.getUserIDFromName(name);
			driver.addMissionAccuracyAverageToStats(id, missionAccuracyAverage);
		}
	}
	public UserProfileData getUserProfileData(String name)
	{
		if (driver.doesUserExistByName(name))
		{
			int id = driver.getUserIDFromName(name);
			return driver.getUserProfileByID(id);
		}
		return null;
	}
	public UserGameStatsData getUserGameStatsData(String name)
	{
		if (driver.doesUserExistByName(name))
		{
			int id = driver.getUserIDFromName(name);
			return driver.getUserGameStatsByID(id);
		}
		return null;
	}
}