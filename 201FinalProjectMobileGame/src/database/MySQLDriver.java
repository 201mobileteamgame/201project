package database;

import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import com.mysql.jdbc.Driver;

public class MySQLDriver {
	
	private Connection con;
	private final static String selectNameUserProfiles = "SELECT * FROM USERPROFILES WHERE userName=?";
	private final static String selectIDUserProfiles = "SELECT * FROM USERPROFILES WHERE userID=?";
	private final static String selectAllUserProfiles = "SELECT * FROM USERPROFILES";
	private final static String addUserProfile = "INSERT INTO userprofiles(userID,userName,userPass,userPassHint) VALUES(?,?,?,?)";
	private final static String deleteIDUserProfile = "DELETE FROM USERPROFILES WHERE userID = ?";
	private final static String deleteNameUserProfile = "DELETE FROM USERPROFILES WHERE userName = ?";
	private final static String addBasicGameStats = "INSERT INTO STATS(userID) VALUES(?)";
	private final static String addAverageScoreToStats = "UPDATE STATS SET averageScore=? WHERE userID = ?";
	private final static String addHighScoreToStats = "UPDATE STATS SET highScore=? WHERE userID = ?";
	private final static String addTotalScoreToStats = "UPDATE STATS SET totalScore=? WHERE userID = ?";
	private final static String addNumberGamesPlayedToStats = "UPDATE STATS SET numberGamesPlayed=? WHERE userID = ?";
	private final static String addNumberLeaderApprovalsToStats = "UPDATE STATS SET numberLeaderApprovals=? WHERE userID = ?";
	private final static String addNumberLeaderRejectionsToStats = "UPDATE STATS SET numberLeaderRejections=? WHERE userID = ?";
	private final static String addNumberWinsToStats = "UPDATE STATS SET numberWins=? WHERE userID = ?";
	private final static String addNumberLossesToStats = "UPDATE STATS SET numberLosses=? WHERE userID = ?";
	private final static String addNumberTimesPatriotToStats = "UPDATE STATS SET numberTimesPatriot=? WHERE userID = ?";
	private final static String addNumberTimesTraitorToStats = "UPDATE STATS SET numberTimesTraitor=? WHERE userID = ?";
	private final static String addQuickestSabotageToStats = "UPDATE STATS SET QuickestSabotage=? WHERE userID = ?";
	private final static String addLongestSabotageToStats = "UPDATE STATS SET longestSabotage=? WHERE userID = ?";
	private final static String addMissionAccuracyAverageToStats = "UPDATE STATS SET missionAccuracyAverage=? WHERE userID = ?";
		
	public MySQLDriver() {
		try {
			new Driver();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	public void connect()
	{
		try {
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/gamestats?user=root&password=root");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	public void stop() 
	{
		try {
			con.close();
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
	}
	public boolean doesUserExistByName(String name)
	{
		try {
			PreparedStatement ps = con.prepareStatement(selectNameUserProfiles);
			ps.setString(1, name);
			ResultSet result = ps.executeQuery();
			while (result.next())
			{
				return true;
			}
		} catch (SQLException e) {e.printStackTrace();}
		return false;
	}
	public boolean doesUserExistByID(int userID)
	{
		try {
			PreparedStatement ps = con.prepareStatement(selectIDUserProfiles);
			ps.setInt(1, userID);
			ResultSet result = ps.executeQuery();
			while (result.next())
			{
				return true;
			}
		} catch (SQLException e) {e.printStackTrace();}
		return false;
	}
	public int getUserIDFromName(String userName)
	{
		if (doesUserExistByName(userName))
		{	
			try
			{
				PreparedStatement ps = con.prepareStatement(selectNameUserProfiles);
				ps.setString(1, userName);
				ResultSet rs = ps.executeQuery();
				while (rs.next())
				{
					int id = rs.getInt("userID");
					return id;
					//String name = rs.getString("userName");
					//System.out.format("%s, %s\n", id, name);
				}
			}
			catch (Exception e)
			{
				System.err.println(e.getMessage());
			}
		}
		return -1;
	}
	public String getUserNameFromID(int userID)
	{
		if (doesUserExistByID(userID))
		{	
			try
			{
				PreparedStatement ps = con.prepareStatement(selectIDUserProfiles);
				ps.setInt(1, userID);
				ResultSet rs = ps.executeQuery();
				while (rs.next())
				{
					int id = rs.getInt("userID");
					String name = rs.getString("userName");
					return name;
					//System.out.format("%s, %s\n", id, name);
				}
			}
			catch (Exception e)
			{
				System.err.println(e.getMessage());
			}
		}
		return "-1";
	}
	public UserProfileData getUserProfileByID(int userID)
	{
		if (doesUserExistByID(userID))
		{	
			try
			{
				PreparedStatement ps = con.prepareStatement(selectIDUserProfiles);
				ps.setInt(1, userID);
				ResultSet rs = ps.executeQuery();
				while (rs.next())
				{
					UserProfileData temp = new UserProfileData();
					temp.id = rs.getInt("userID");
					temp.username = rs.getString("userName");
					temp.hashedPassword = rs.getString("userPass");
					temp.passwordHint = rs.getString("userPassHint");
					return temp;
				}
			}
			catch (Exception e)
			{
				System.err.println(e.getMessage());
			}
		}
		return null;
	}
	
	public UserProfileData getUserProfileByName(String userName)
	{
		if (doesUserExistByName(userName))
		{
			try
			{
				PreparedStatement ps = con.prepareStatement(selectNameUserProfiles);
				ps.setString(1, userName);
				ResultSet rs = ps.executeQuery();
				while (rs.next())
				{
					UserProfileData temp = new UserProfileData();
					temp.id = rs.getInt("userID");
					temp.username = rs.getString("userName");
					temp.hashedPassword = rs.getString("userPass");
					temp.passwordHint = rs.getString("userPassHint");
					return temp;
				}
			}
			catch (Exception e)
			{
				System.err.println(e.getMessage());
			}
		}
		return null;
	}
	public UserGameStatsData getUserGameStatsByID(int userID)
	{
		if (doesUserExistByID(userID))
		{
			try
			{
				PreparedStatement ps = con.prepareStatement(selectIDUserProfiles);
				ps.setInt(1, userID);
				ResultSet rs = ps.executeQuery();
				while (rs.next())
				{
					UserGameStatsData temp = new UserGameStatsData();
					temp.id = rs.getInt("userID");
					temp.averageScore = rs.getDouble("AverageScore");
					if (rs.wasNull()){temp.averageScore = -1;}
					temp.highScore = rs.getInt("HighScore");
					if (rs.wasNull()){temp.highScore = -1;}
					temp.totalScore = rs.getInt("TotalScore");
					if (rs.wasNull()){temp.totalScore = 0;}
					temp.gamesPlayed = rs.getInt("NumberGamesPlayed");
					if (rs.wasNull()){temp.gamesPlayed = 0;}
					temp.leaderApprovals = rs.getInt("NumberLeaderApprovals");
					if (rs.wasNull()){temp.leaderApprovals = 0;}
					temp.leaderRejections = rs.getInt("NumberLeaderRejections");
					if (rs.wasNull()){temp.leaderRejections = 0;}
					temp.numberWins = rs.getInt("NumberWins");
					if (rs.wasNull()){temp.numberWins = 0;}
					temp.numberLosses = rs.getInt("NumberLosses");
					if (rs.wasNull()){temp.numberLosses = 0;}
					temp.numberTimesPatriot = rs.getInt("NumberTimesPatriot");
					if (rs.wasNull()){temp.numberTimesPatriot = 0;}
					temp.numberTimesTraitor = rs.getInt("NumberTimesTraitor");
					if (rs.wasNull()){temp.numberTimesTraitor = 0;}
					temp.quickestSabotage = rs.getInt("QuickestSabotage");
					if (rs.wasNull()){temp.quickestSabotage = -1;}
					temp.longestSabotage = rs.getInt("LongestSabotage");
					if (rs.wasNull()){temp.longestSabotage = -1;}
					temp.missionAccuracyAverage = rs.getDouble("MissionAccuracyAverage");
					if (rs.wasNull()){temp.missionAccuracyAverage = -1;}
					return temp;
				}
			}
			catch (Exception e)
			{
				System.err.println(e.getMessage());
			}
		}
		return null;
	}
	public void addUserProfile (String name, String pass, String passHint)
	{
		System.out.println("adding user: " + name);
		int id = getNextID();
		try 
		{
			PreparedStatement ps = con.prepareStatement(addUserProfile);
			ps.setString(2, name);
			ps.setInt(1, id);
			ps.setString(3, pass);
			ps.setString(4, passHint);
			ps.executeUpdate();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		addGamerProfile(id);
	}
	private void addGamerProfile(int id)
	{
		try 
		{
			PreparedStatement ps = con.prepareStatement(addBasicGameStats);
			ps.setInt(1, id);
			ps.executeUpdate();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
	}
	public void deleteIDUserProfile (int userID)
	{
		try 
		{
			PreparedStatement ps = con.prepareStatement(deleteIDUserProfile);
			ps.setInt(1, userID);
			ps.executeUpdate();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
	}
	public void deleteNameUserProfile (String userName)
	{
		try 
		{
			PreparedStatement ps = con.prepareStatement(deleteNameUserProfile);
			ps.setString(1, userName);
			ps.executeUpdate();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
	}
	public int getNextID()
	{
		try
		{
			PreparedStatement ps = con.prepareStatement(selectAllUserProfiles);
			ResultSet rs = ps.executeQuery();
			int rows=0;
			while (rs.next())
			{
				rows+=1;
			}
			return (rows+1);
		}
		catch (Exception e)
		{
			System.err.println(e.getMessage());
		}
		return -1;
	}
	public String getUserPassFromID(int userID)
	{
		if (doesUserExistByID(userID))
		{	
			try
			{
				PreparedStatement ps = con.prepareStatement(selectIDUserProfiles);
				ps.setInt(1, userID);
				ResultSet rs = ps.executeQuery();
				while (rs.next())
				{
					int id = rs.getInt("userID");
					String name = rs.getString("userName");
					String pass = rs.getString("userPass");
					return pass;
					//System.out.format("%s, %s\n", id, name);
				}
			}
			catch (Exception e)
			{
				System.err.println(e.getMessage());
			}
		}
		return "-1";
	}
	public String getUserPassFromName(String userName)
	{
		if (doesUserExistByName(userName))
		{	
			try
			{
				PreparedStatement ps = con.prepareStatement(selectNameUserProfiles);
				ps.setString(1, userName);
				ResultSet rs = ps.executeQuery();
				while (rs.next())
				{
					int id = rs.getInt("userID");
					String name = rs.getString("userName");
					String pass = rs.getString("userPass");
					return pass;
					//System.out.format("%s, %s\n", id, name);
				}
			}
			catch (Exception e)
			{
				System.err.println(e.getMessage());
			}
		}
		return "-1";
	}
	public String getUserPassHintFromID(int userID)
	{
		if (doesUserExistByID(userID))
		{	
			try
			{
				PreparedStatement ps = con.prepareStatement(selectIDUserProfiles);
				ps.setInt(1, userID);
				ResultSet rs = ps.executeQuery();
				while (rs.next())
				{
					int id = rs.getInt("userID");
					String name = rs.getString("userName");
					String pass = rs.getString("userPass");
					String passHint = rs.getString("userPassHint");
					return passHint;
					//System.out.format("%s, %s\n", id, name);
				}
			}
			catch (Exception e)
			{
				System.err.println(e.getMessage());
			}
		}
		return "-1";
	}
	public String getUserPassHintFromName(String userName)
	{
		if (doesUserExistByName(userName))
		{	
			try
			{
				PreparedStatement ps = con.prepareStatement(selectNameUserProfiles);
				ps.setString(1, userName);
				ResultSet rs = ps.executeQuery();
				while (rs.next())
				{
					int id = rs.getInt("userID");
					String name = rs.getString("userName");
					String pass = rs.getString("userPass");
					String passHint = rs.getString("userPassHint");
					return passHint;
					//System.out.format("%s, %s\n", id, name);
				}
			}
			catch (Exception e)
			{
				System.err.println(e.getMessage());
			}
		}
		return "-1";
	}
	public void addAverageScoreToStats(int id, double avgScore)
	{
		try 
		{
			PreparedStatement ps = con.prepareStatement(addAverageScoreToStats);
			ps.setDouble(1, avgScore);
			ps.setInt(2, id);
			ps.executeUpdate();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
	}
	public void addHighScoreToStats(int id, int highScore)
	{
		try 
		{
			PreparedStatement ps = con.prepareStatement(addHighScoreToStats);
			ps.setInt(1, highScore);
			ps.setInt(2, id);
			ps.executeUpdate();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
	}
	public void addTotalScoreToStats(int id, int totalScore)
	{
		try 
		{
			PreparedStatement ps = con.prepareStatement(addTotalScoreToStats);
			ps.setInt(1, totalScore);
			ps.setInt(2, id);
			ps.executeUpdate();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
	}
	public void addNumberGamesPlayedToStats(int id, int gamesPlayed)
	{
		try 
		{
			PreparedStatement ps = con.prepareStatement(addNumberGamesPlayedToStats);
			ps.setInt(1, gamesPlayed);
			ps.setInt(2, id);
			ps.executeUpdate();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
	}
	
	public void addNumberLeaderApprovalsToStats(int id, int numberApprovals)
	{
		try 
		{
			PreparedStatement ps = con.prepareStatement(addNumberLeaderApprovalsToStats);
			ps.setInt(1, numberApprovals);
			ps.setInt(2, id);
			ps.executeUpdate();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
	}
	
	public void addNumberLeaderRejectionsToStats(int id, int numberRejections)
	{
		try 
		{
			PreparedStatement ps = con.prepareStatement(addNumberLeaderRejectionsToStats);
			ps.setInt(1, numberRejections);
			ps.setInt(2, id);
			ps.executeUpdate();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
	}
	
	public void addNumberWinsToStats(int id, int numberWins)
	{
		try 
		{
			PreparedStatement ps = con.prepareStatement(addNumberWinsToStats);
			ps.setInt(1, numberWins);
			ps.setInt(2, id);
			ps.executeUpdate();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
	}
	
	public void addNumberLossesToStats(int id, int numberLosses)
	{
		try 
		{
			PreparedStatement ps = con.prepareStatement(addNumberLossesToStats);
			ps.setInt(1, numberLosses);
			ps.setInt(2, id);
			ps.executeUpdate();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
	}
	
	public void addNumberTimesPatriotToStats(int id, int timesPatriot)
	{
		try 
		{
			PreparedStatement ps = con.prepareStatement(addNumberTimesPatriotToStats);
			ps.setInt(1, timesPatriot);
			ps.setInt(2, id);
			ps.executeUpdate();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
	}
	
	public void addNumberTimesTraitorToStats(int id, int timesTraitor)
	{
		try 
		{
			PreparedStatement ps = con.prepareStatement(addNumberTimesTraitorToStats);
			ps.setInt(1, timesTraitor);
			ps.setInt(2, id);
			ps.executeUpdate();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
	}
	
	public void addQuickestSabotageToStats(int id, int quickestSabotage)
	{
		try 
		{
			PreparedStatement ps = con.prepareStatement(addQuickestSabotageToStats);
			ps.setInt(1, quickestSabotage);
			ps.setInt(2, id);
			ps.executeUpdate();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
	}
	
	public void addLongestSabotageToStats(int id, int longestSabotage)
	{
		try 
		{
			PreparedStatement ps = con.prepareStatement(addLongestSabotageToStats);
			ps.setInt(1, longestSabotage);
			ps.setInt(2, id);
			ps.executeUpdate();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
	}
	public void addMissionAccuracyAverageToStats(int id, double missionAccuracyAverage)
	{
		try 
		{
			PreparedStatement ps = con.prepareStatement(addMissionAccuracyAverageToStats);
			ps.setDouble(1, missionAccuracyAverage);
			ps.setInt(2, id);
			ps.executeUpdate();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
	}
}
