package database;

public class UserGameStatsData 
{
	public int id;
	public double averageScore;
	public int highScore;
	public int totalScore;
	public int gamesPlayed;
	public int leaderApprovals;
	public int leaderRejections;
	public int numberWins;
	public int numberLosses;
	public int numberTimesPatriot;
	public int numberTimesTraitor;
	public int quickestSabotage;
	public int longestSabotage;
	public double missionAccuracyAverage;
}
