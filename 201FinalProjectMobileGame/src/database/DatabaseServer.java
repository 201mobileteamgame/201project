package database;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Vector;

import game.Player;




public class DatabaseServer extends Thread
{

	private Socket s;
	private ServerSocket ss;
	private Vector<DatabaseServerThread> ptVector;
	private int mPortNumber;
	private int mNumPlayers;
	DatabaseCommunicator dbCom;
	
	public DatabaseServer(int portNumber) {
		ptVector = new Vector<DatabaseServerThread>();
		mPortNumber = portNumber;
		dbCom = new DatabaseCommunicator();
		start();	
	}
	
	public void sendMessageToAllClients(String message) {
		System.out.println(message);
		for(DatabaseServerThread pt : ptVector) { //How to iterate through a vector, can also use i = 0
				pt.sendObject((String)message);
		}
	}
	public void relayMessageToAllClients(String message, DatabaseServerThread sendingThread) {
		System.out.println(message);
		for(DatabaseServerThread pt : ptVector) { //How to iterate through a vector, can also use i = 0
			if (sendingThread != pt) {
				pt.sendObject((String)message);
			}
		}
	}
	public void sendObjectToClient(int identifier,Object obj) {
		for(DatabaseServerThread pt : ptVector) { //How to iterate through a vector, can also use i = 0
			if (identifier == pt.getId())
				pt.sendObject(obj);
		}
	}
	public void processLogin(int identifier,String userAndPass)
	{
		Player upd;
		if ((upd = dbCom.login(userAndPass.split("/")[0],userAndPass.split("/")[1]))!=null)
			sendObjectToClient(identifier,upd);
	}
	public void registerUser(String userInfo)
	{
		System.out.println("Server adding user: "+userInfo);
		dbCom.createNewUser(userInfo.split("/")[0], userInfo.split("/")[1], userInfo.split("/")[2]);
	}
	
	/*
	public void sendChatMessageEverywhere(int identifier, String message)
	{
		String color;
		if (identifier!=0)
			color = colorSelected.get(identifier-1);
		else
			color = "Main";
		String sendMessage = "CHATMESSAGE:";
		sendMessage+= color+"/";
		sendMessage+=message;
		sendMessageToAllClients(sendMessage);
	}
	*/
	public void run()
	{
		//Connecting everyone
		try {
			ss = new ServerSocket(mPortNumber);
			int i=0;
			while(true) {  //allows as many clients as possible
				Socket s = ss.accept();
				System.out.println("Client " + (i+1) + " Connected: " + s.getInetAddress());
				DatabaseServerThread pt = new DatabaseServerThread(s,this,i+1);
				ptVector.add(pt);
				i++;
			}
		} catch (IOException ioe) {
			System.out.println("ioe in ChatServer constructor: " + ioe.getMessage());
		}
		System.out.println("Every Client is Set up");
		//sendMessageToAllClients("Clients are Set up");
		
	}	
	public static void main (String []args)
	{
		DatabaseServer db = new DatabaseServer(1111);
	}
}
