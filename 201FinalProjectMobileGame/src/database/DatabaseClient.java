package database;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;
import game.*;
import gameGUI.GameWindow;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class DatabaseClient extends Thread {
	
	private Socket mSocket;
	private GameWindow mClientGUI;
	private String mHostName;
	private ObjectOutputStream oos;
	private ObjectInputStream ois;
	private int mPort;
	private GameWindow mGameWindow;
	public int identifier;

	public DatabaseClient(String hostname,GameWindow gw) {
		mHostName =hostname;
		mPort = 1111;
		mGameWindow = gw;
		boolean socketReady = initializeVariables();
		if (socketReady) {
			start();
		}
	}
	private boolean initializeVariables() 
	{
		try 
		{
			mSocket = new Socket(mHostName, mPort);
			oos = new ObjectOutputStream(mSocket.getOutputStream());
			ois = new ObjectInputStream(mSocket.getInputStream());
		} 
		catch (IOException ioe) 
		{
			Util.printExceptionToCommand(ioe);
			return false;
		}
		return true;
	}
	public void sendObject(Object obj) {
		System.out.println("Sending Object: "+obj.toString());
		try 
		{
			if(oos != null) 
			{
				oos.writeObject(obj);
				oos.flush();
			}
		} 
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} //Always need to flush after printing a line
	}
	public Object receiveObject()
	{
		Object obj=null;
		try {
			obj = ois.readObject();
		} catch (ClassNotFoundException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (obj==null)
		{
			System.out.println("Object is not received");
		}
		return obj;
	}
	public void parseMessage(String message)
	{
		/*if (message.split(":",2)[0].equals("MOVETOGAME"))
		{
			String[] messageSplit =message.split(":",2) ;
			String command = messageSplit[0];
			String notCommand = messageSplit[1];
			mClientPanel.moveToGamePanel(Integer.parseInt(notCommand));
		}
		else if (message.split(":",2)[0].equals("ColorSelection"))
		{
			String[] messageSplit =message.split(":",2) ;
			String command = messageSplit[0];
			String notCommand = messageSplit[1];
			mClientPanel.updateColorButtons(notCommand);
		}
		else if (message.split(":",2)[0].equals("CHATMESSAGE"))
		{
			System.out.println("Chat Message Received");
			String[] messageSplit =message.split(":",2) ;
			String command = messageSplit[0];
			String notCommand = messageSplit[1];
			mClientPanel.updateChatBox(notCommand);
		}
		*/
	}
	public void parseObject(Object obj)
	{
		if (obj instanceof Player)
			mGameWindow.getLoginPanel().successfulLogin((Player)obj);
		if (obj==null)
			mGameWindow.getLoginPanel().failedLogin();
			
	}
	public void run() 
	{
		Object obj = receiveObject();
		if (obj instanceof String)
		{
			String message = (String)obj;
			identifier = Integer.parseInt(message.split(":")[1]);
			System.out.println("Received identifier: "+message);
		}
		sendObject("Client "+identifier + "has received confirmation");
		while(true) {
			Object received = (Object)receiveObject();
			/*if (received == null) {
				break;
			}*/
			if (received instanceof String)
				parseMessage((String)received);
			else
				parseObject(received);
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
		/*try {
			while(true) { //Getting lines from the server
				String message = br.readLine();
				if (message == null) {
					break;
				}
				System.out.println(message);
			}
		} catch(IOException ioe) {
			System.out.println("ioe in ChatClient.run(): " + ioe.getMessage());
		}
		*/
	
		//public void run() {
			/*try {
				mFClientGUI.addMessage(Constants.waitingForFactoryConfigMessage);
				Factory factory;
				while(true) {
					// in case the server sends another factory to us
					Object obj = ois.readObject();
					if (obj instanceof Factory)
					{
						factory = (Factory)obj;
						mFManager.loadFactory(factory, mFClientGUI.getTable());
						mFClientGUI.addMessage(Constants.factoryReceived);
						mFClientGUI.addMessage(factory.toString());
					} else if (obj instanceof Resource) {
						Resource toDeliver = (Resource)obj;
						mFManager.deliver(toDeliver);
						mFClientGUI.addMessage(Constants.resourceReceived);
						mFClientGUI.addMessage(toDeliver.toString());
					}
					
				}
			} catch (IOException ioe) {
				mFClientGUI.addMessage(Constants.serverCommunicationFailed);
			} catch (ClassNotFoundException cnfe) {
				Util.printExceptionToCommand(cnfe);
			}
			*/
		//}
}

