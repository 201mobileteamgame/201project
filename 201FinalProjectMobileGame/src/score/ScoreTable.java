package score;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;

import customUI.PaintedButton;
import customUI.PaintedPanel;
import gameGUI.GameWindow.SwitchPageAction;
import library.FontLibrary;
import library.ImageLibrary;

public class ScoreTable extends PaintedPanel {
	private static final long serialVersionUID = 1L;
	
	private JTabbedPane teamTP;
	
	private JScrollPane patriotSP;
	private static JTable patriotTable;
	private static DefaultTableModel patriotTableModel;
	
	private JScrollPane traitorSP;
	private static JTable traitorTable;
	private static DefaultTableModel traitorTableModel;
	private String[] columns = {"Team", "Rounds"};
	
	private PaintedButton backButton;
	
	private JPanel topEmptyPanel;
	private JPanel bottomEmptyPanel;
	private JPanel westEmptyPanel;
	private JPanel eastEmptyPanel;
	
	SwitchPageAction backAction;
	
	public ScoreTable(SwitchPageAction inBackAction) {
		backAction = inBackAction;
		initializeComponents();
		createGUI();
		addEvents();
	}

	private void initializeComponents() {
		teamTP = new JTabbedPane();
		patriotTableModel = new DefaultTableModel(columns, 11) {
			private static final long serialVersionUID = 1L;
			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		patriotTable = new JTable(patriotTableModel);
		patriotSP = new JScrollPane(patriotTable);
		
		traitorTableModel = new DefaultTableModel(columns, 11) {
			private static final long serialVersionUID = 1L;
			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		traitorTable = new JTable(traitorTableModel);
		traitorSP = new JScrollPane(traitorTable);
		
		backButton = new PaintedButton("Back",
				FontLibrary.getFont("resources/fonts/typewriter.ttf", Font.BOLD, 14),
				ImageLibrary.getImage("resources/images/buttons/red_button00.png")
				);
		
		topEmptyPanel = new JPanel();
		bottomEmptyPanel = new JPanel();
		westEmptyPanel = new JPanel();
		eastEmptyPanel = new JPanel();
	}

	private void createGUI() {
		setLayout(new BorderLayout());
		setOpaque(false);
		
		patriotTable.setRowHeight(40);
		patriotTable.setFont(new Font("Zapfino", Font.BOLD, 30));
		patriotTable.setCellSelectionEnabled(false);
		patriotTable.setGridColor(Color.BLUE);
		
		traitorTable.setRowHeight(40);
		traitorTable.setFont(new Font("Zapfino", Font.BOLD, 30));
		traitorTable.setCellSelectionEnabled(false);
		traitorTable.setGridColor(Color.RED);
		
		teamTP.add("Spies",patriotSP);
		teamTP.add("Double Agents",traitorSP);
		teamTP.setFont(FontLibrary.getFont("resources/fonts/typewriter.ttf", Font.ITALIC, 20));
		patriotSP.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		traitorSP.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		
		JTableHeader patriotHeader = patriotTable.getTableHeader();
		JTableHeader traitorHeader = traitorTable.getTableHeader();
		patriotHeader.setPreferredSize(new Dimension(patriotSP.getWidth(),40));
		traitorHeader.setPreferredSize(new Dimension(traitorSP.getWidth(),40));
		patriotHeader.setBackground(Color.BLUE);
		patriotHeader.setForeground(Color.WHITE);
		traitorHeader.setBackground(Color.RED);
		traitorHeader.setForeground(Color.BLACK);
		patriotHeader.setFont(FontLibrary.getFont("resources/fonts/typewriter.ttf", Font.BOLD, 20));
		traitorHeader.setFont(FontLibrary.getFont("resources/fonts/typewriter.ttf", Font.BOLD, 20));
		
		teamTP.setBackgroundAt(0, Color.BLUE);
		teamTP.setForegroundAt(0, Color.WHITE);
		teamTP.setBackgroundAt(1, Color.RED);
		teamTP.setForegroundAt(1, Color.BLACK);
		teamTP.setOpaque(false);
		
		topEmptyPanel.setPreferredSize(new Dimension(520,110));
		topEmptyPanel.setOpaque(false);
		
		bottomEmptyPanel.setPreferredSize(new Dimension(520,130));
		bottomEmptyPanel.setOpaque(false);
		
		westEmptyPanel.setPreferredSize(new Dimension(80,920));
		westEmptyPanel.setOpaque(false);
		
		eastEmptyPanel.setPreferredSize(new Dimension(80,920));
		eastEmptyPanel.setOpaque(false);
		
		bottomEmptyPanel.add(backButton);
		
		add(topEmptyPanel,BorderLayout.NORTH);
		add(bottomEmptyPanel,BorderLayout.SOUTH);
		add(westEmptyPanel,BorderLayout.WEST);
		add(eastEmptyPanel,BorderLayout.EAST);
		add(teamTP,BorderLayout.CENTER);
	}
	
	private void addEvents() {
		backButton.addActionListener(backAction);
		
	}
}
