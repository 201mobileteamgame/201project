package game;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;

import server.Server;

public class GameManager {
	
	private int numPlayers;
	private Vector<Player> gamePlayers;
	private Vector<String> squadStrings;
	private Vector<Player> missionPlayers;
	//private Vector<Player> nonMissionPlayers;
	private Vector<Player> traitors;
	private int leaderOrder;
	private Player leader;
	
	private GameConstants gc;
	
	//Game Flow
	private Phase currentPhase;
	private MissionGame currentMission;
	private int missionNum;
	private int roundNum;
	private int failedMissions;
	private int failedVotes;
	
	//Team voting
	private int votes = 0;
	private int reject = -1;
	
	private boolean teamApproved = false;
	
	private Server server;
	
	{
		leaderOrder = -1;
		missionPlayers = new Vector<Player>();
		//nonMissionPlayers = new Vector<Player>();
		traitors = new Vector<Player>();
	}
	
	public GameManager() {
		
	}
	
	//Take in vector created in the wait page
	public void setUp(Vector<Player> playerVect, Server s) {
		System.out.println();
		System.out.println("Number of players: "+playerVect.size());
		for(Player p : playerVect) {
			System.out.println("In game players: "+p.getPlayerName());
		}
		System.out.println();
		//setup
		numPlayers = playerVect.size();
		gamePlayers = playerVect;
		server = s;
		
		currentPhase = Phase.SQUAD;
		missionNum = 0;
		roundNum = 0;
		failedMissions = 0;
		failedVotes = 0;
		
		//setup constants
		gc = new GameConstants(numPlayers);
		if(gc.getTraitorCount()==0) server.sendMessage("Error: InvalidGameSize");
		
		//setup traitors
		Random generator = new Random();
		for(int i=0; i<gc.getTraitorCount(); i++){
			int rn = generator.nextInt(numPlayers);
			if(gamePlayers.get(rn).isTraitor()) i--;
			else {
				gamePlayers.get(rn).setTraitor(true);
				traitors.add(gamePlayers.get(rn));
			}
		}
		server.sendMessage("Traitors:"+writeTraitorNames());
		
		//pick new leader
		setNewLeader();
	}
	
	//Setting the leader in order, and back to first player if vector reaches end
	private void setNewLeader() {
		//increment leaderOrder for a new Leader
		if(leaderOrder==(numPlayers-1)) leaderOrder = 0;
		else leaderOrder+=1;
			
		//Match leaderOrder to the corresponding player order in the gamePlayers vector
		//Assign the player leader
		int count = 0;
		for(Player p : gamePlayers) {
			if(leaderOrder==count) {
				p.setLeader(true);
				leader = p;
			}
			else {
				p.setLeader(false);
			}
			++count;
		}

		server.sendMessage("StartSquad: "+leader.getPlayerName());
	}
	
	//The passed in vector should be read in from client-sent messages
	public void setMissionPlayers(Vector<Player> mPlayers) {
		//Remove previous mission players from the mission vector
		//Vector is only empty when first entering the game
		if(!missionPlayers.isEmpty()) {
			for(Player p : missionPlayers) {
				p.setOnMission(false);
			}
			missionPlayers.removeAllElements();
		}
		
		missionPlayers = mPlayers;
		for(Player p : missionPlayers) {
			p.setOnMission(true);
		}
	}
	
	public MissionGame getCurrentMission() {
		return currentMission;
	}
	
	public void callFreeze() {
		server.sendMessage("Freeze");
	}
	
	private boolean checkTeamApproval() {
		System.out.println("Checking Team Approval");
		if(reject*2 >= numPlayers) {
			return false;
		}
		else {
			populateMissionPlayers();
			return true;
		}
	}
	
	private void populateMissionPlayers() {
		missionPlayers.removeAllElements();
		for(String missionName : squadStrings) {
			for(Player p : gamePlayers) {
				if(p.getPlayerName().equals(missionName)) {
					missionPlayers.add(p);
				}
			}
		}
	}
	
	public void addVote(boolean vote) {
		votes++;
		System.out.println("Total players: "+numPlayers+" current Votes: "+votes);
		if(!vote) reject+=1;
		if(votes>=numPlayers) phaseTransition();
		System.out.println("Reject count is: "+reject);
	}
	
	public void setSquad(Vector<String> inSquad){
		squadStrings = inSquad;
		phaseTransition();
	}
	
	private Vector<Player> convertSquadStringToPlayers(){

		Vector<Player> squadPlayers = new Vector<Player>();
		
		for(String s : squadStrings) {
			for(Player p : gamePlayers) {
				if (p.getPlayerName()==s){
					squadPlayers.add(p);
				}
			}
		}
		
		return squadPlayers;
	}
	
//	private void startTimer(long duration) {
//		Timer phaseTimer = new Timer();
//		phaseTimer.schedule(new TimerTask(){
//			@Override
//			public void run(){
//				phaseTransition();
//			}
//		}, duration);
//	}
	
	private String writeSquadNames(){
		String squadNames = "";
		for(String s : squadStrings) {
			squadNames = squadNames + " " + s;
		}
		return squadNames;
	}
	
	private String writeTraitorNames(){
		String traitorNames = "";
		for(Player p : traitors) {
			traitorNames = traitorNames + " " + p.getPlayerName();
		}
		return traitorNames;
	}
	
	private String writeGameState(){
		String gameState = "Mission# " + missionNum + " Round# " + roundNum + " FailedMissions# " + failedMissions + " FailedVote# " + failedVotes;
		return gameState;
	}
	
	public void phaseTransition() {
		System.out.println();
		System.out.println("Current phase is "+currentPhase);
		System.out.println();
		switch(currentPhase) {
			case SQUAD:		System.out.println("SQUAD  PHASE: "+writeSquadNames());
							server.sendMessage("StartVote:"+writeSquadNames());
				//			server.sendMessage("StartVote: Guest2 Guest3 Guest4 Guest5"); //Testing
							setMissionPlayers(convertSquadStringToPlayers());
							currentPhase = Phase.VOTE;
							votes = 0;
							reject = 0;
							//startTimer(gc.getPhaseDuration(1));
							break;
		
			case VOTE:		System.out.println("Vote Phase Start");
							server.sendMessage("StopVoting");
							teamApproved = false;
							if(checkTeamApproval()) {
								failedVotes = 0;
								System.out.println("Create new mission here");
								currentMission = new MissionGame(missionPlayers,gc.taskCount[missionNum],server,this);
								server.sendMessage("StartMission");
								currentPhase = Phase.MISSION;
								teamApproved = true;
								//startTimer(gc.getPhaseDuration(2));
							} else {
								server.sendMessage("StartResult: "+writeGameState());
								failedVotes++;
//								currentPhase = Phase.RESULT;
								currentPhase = Phase.MISSION;
								phaseTransition();
								//startTimer(gc.getPhaseDuration(3));
							}
							
							break;
						
			case MISSION:	if(teamApproved) {
								if(!currentMission.checkResult()) {
									failedMissions++;
									server.sendMessage("TraitorsWinMission");
								}
								else {
									server.sendMessage("PatriotsWinMission");
								}
								server.sendMessage("StartResult: "+writeGameState());
								missionNum++;
						//		currentPhase = Phase.RESULT;
							}
							if((failedMissions >= 3)||(failedVotes >= 5)) {server.sendMessage("TraitorVictory: "+writeGameState());}
							else if (missionNum-failedMissions >= 3) {server.sendMessage("PatriotVictory: "+writeGameState());}
							else {
								roundNum++;
								setNewLeader();
								currentPhase = Phase.SQUAD;
							}
							//startTimer(gc.getPhaseDuration(3));
							break;
							
//			case RESULT:	if((failedMissions >= 3)||(failedVotes >= 5)) {server.sendMessage("TraitorVictory: "+writeGameState());}
//							else if (missionNum-failedMissions >= 3) {server.sendMessage("PatriotVictory: "+writeGameState());}
//							else {roundNum++;}
//							setNewLeader();
//							currentPhase = Phase.SQUAD;
							//startTimer(gc.getPhaseDuration(0));
//							break;
							
			default:		server.sendMessage("Error: Phase Lost");
							break;
		}
	}
	
}
