package game;

import java.util.ArrayList;
import java.util.Stack;
import java.util.Vector;

import server.Server;

public class MissionGame {
	private Vector<Player> onMissionPlayers;
	private Vector<TaskTrack> assignedTask;
	private Stack<Task> taskStack;
	private TaskManager taskManager;
	private GameManager gameManager;
	
	private boolean missionResult = false;
	
	private Server server;
	
	private int taskToComplete;

	public MissionGame(Vector<Player> players, int tasknum, Server server, GameManager ingm) {
		onMissionPlayers = players;
		assignedTask = new Vector<TaskTrack>();
		taskStack = new Stack<Task>();
		taskToComplete = tasknum;
		server.sendMessage("TotalNumTasks: " + taskToComplete);
		this.server = server;
		gameManager = ingm;
		taskManager = new TaskManager();
		
		//Shuffle tasks and add to stack
		int count = 0;
		while(count<taskToComplete) {
			ArrayList<Task> toAddTaskList = taskManager.getShuffledTaskList();
			for(Task toAddTask : toAddTaskList) {
				taskStack.push(toAddTask);
				count++;
				if(count>=taskToComplete) break;
			}
		}
		
		for(int i=0;i<16;i++) {
			TaskTrack taskTrack = new TaskTrack(i);
			assignedTask.addElement(taskTrack);
		}
		
		new ShortTimer(64);
	}
	
	//playerID --> player who completed the task
	//needTaskPlayer --> player who had the instruction for the completed task
	//needTaskPlayer will receive a new task instruction
	public void receiveDoneTask(int doneTask, String playerName) {
		boolean taskExist = false;
		for(TaskTrack track : assignedTask) {
			if(track.getButton()==doneTask) {
				if(!track.getAssignedPlayer().isEmpty()) {
					//At least one player had this instruction
					Player needTaskPlayer = track.removeAssignedPlayer();
					
					taskExist = true;
					//TODO check if all tasks are done
//					if(taskStack.isEmpty()) {
//						boolean allTaskDone = true;
//						for(TaskTrack tt : assignedTask) {
//							if(tt.getAssignedPlayer()!=null) {
//								allTaskDone = false;
//							}
//						}
//						if(allTaskDone) {
//							System.out.println("ALL TASKS ARE DONE...");
//							missionResult = true;
//							return;
//						}
//					}
					
					if(!taskStack.isEmpty()) {
						Task newTask = taskStack.pop();
						//Send task instruction to player
						server.sendMessage("NewTask: "+needTaskPlayer.getPlayerName()+" "+newTask.getInstruction());
						for(TaskTrack newTrack : assignedTask) {
							if(newTrack.getButton()==newTask.getButton()) {
								newTrack.setAssignedPlayer(needTaskPlayer);
							}
						}
					}
					else {
						//TODO manage receive
						server.sendMessage("NewTask: "+needTaskPlayer.getPlayerName()+" Mission Completed");
					}
					
				}
				break;
			}
		}
		
		if(!taskExist) {
			//TODO manage button misClicks for the playerName
			for(Player player : onMissionPlayers) {
				if((player.getPlayerName()).equals(playerName)) {
					player.addMisclick();
					if(player.checkMisclick()) {
						gameManager.callFreeze();
					}
				}
			}
			server.sendMessage("Misclick: "+playerName);
		} 
		else {
			//TODO send completed task signal to progress bar
			server.sendMessage("Completed");
		}
		
	}
	
	public boolean checkResult() {
		if(taskStack.isEmpty()) {
			boolean allTaskDone = true;
			for(TaskTrack tt : assignedTask) {
				if(!tt.getAssignedPlayer().isEmpty()) {
					allTaskDone = false;
				}
			}
			if(allTaskDone) {
				System.out.println("ALL TASKS ARE DONE...");
				return true;
			}
		}
		return false;
	}
	
	
	//Tracks which player each task instruction is printed to
	class TaskTrack {
		private final int button;
		private Stack<Player> players;
		
		public TaskTrack(int buttonNum) {
			button = buttonNum;
			players = new Stack<Player>();
		}
		
		public int getButton() {
			return button;
		}
		
		public Stack<Player> getAssignedPlayer() {
			return players;
		}
		
		public void setAssignedPlayer(Player p) {
			players.push(p);
		}
		
		public Player removeAssignedPlayer() {
			return players.pop();
		}
	}
	
	private void sendInitTasks() {
		for(Player player : onMissionPlayers) {
			Task newTask = taskStack.pop();
			//Send task instruction to player
			server.sendMessage("NewTask: "+player.getPlayerName()+" "+newTask.getInstruction());
			for(TaskTrack newTrack : assignedTask) {
				if(newTrack.getButton()==newTask.getButton()) {
					newTrack.setAssignedPlayer(player);
				}
			}
		}
	}
	
	class ShortTimer extends Thread {
		private long startTime;
		private long currentTime;
		private int maxTime = -1;
		private int timePassed = -1;
		
		public ShortTimer(int time) {
			startTime = System.currentTimeMillis();
			currentTime = startTime;
			maxTime = time;
			this.start();
		}
		
		public void run() {
			boolean initTasksSent = false;
			while(!timesUp()) {
				if(getTimePassed()==4 && initTasksSent==false) {
					sendInitTasks();
					initTasksSent = true;
				}
				currentTime = System.currentTimeMillis();
				timePassed = (int) (currentTime-startTime)/1000;
				try {
					Thread.sleep(200);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			server.sendMessage("StopMission");
			gameManager.phaseTransition();
		}
		
		public boolean timesUp() {
			return timePassed>=maxTime;
		}
		
		public int getTimePassed() {
			return timePassed;
		}
	}
	
}
