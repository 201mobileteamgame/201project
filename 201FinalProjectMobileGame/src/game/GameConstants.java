package game;

public class GameConstants {

	public int traitorCount;
	public int[] squadSize;
	public long[] phaseDuration;
	public int[] missionDuration;
	public int[] taskCount;
	
	public GameConstants(int playerCount){
		switch(playerCount){
			case 5: traitorCount = 1;
					squadSize = new int[]{2,3,2,3,3};
					phaseDuration = new long[]{1500,1500,1500,1500};
					missionDuration = new int[]{3000,3000,3000,3000,3000};
					taskCount = new int[]{25,30,30,35,35};
					break;
					
			case 6: traitorCount = 2;
					squadSize = new int[]{2,3,4,3,4};
					phaseDuration = new long[]{1500,1500,1500,1500};
					missionDuration = new int[]{3000,3000,3000,3000,3000};
					taskCount = new int[]{25,30,30,35,35};
					break;
					
			case 7: traitorCount = 3;
					squadSize = new int[]{2,3,3,4,4};
					phaseDuration = new long[]{1500,1500,1500,1500};
					missionDuration = new int[]{3000,3000,3000,4500,3000};
					taskCount = new int[]{25,30,30,35,35};
					break;
					
			case 8: traitorCount = 3;
					squadSize = new int[]{3,4,4,5,5};
					phaseDuration = new long[]{1500,1500,1500,1500};
					missionDuration = new int[]{3000,3000,3000,4500,3000};
					taskCount = new int[]{25,30,30,35,35};
					break;
					
			case 9: traitorCount = 3;
					squadSize = new int[]{3,4,4,5,5};
					phaseDuration = new long[]{1500,1500,1500,1500};
					missionDuration = new int[]{3000,3000,3000,4500,3000};
					taskCount = new int[]{25,30,30,35,35};
					break;
					
			case 10: traitorCount = 4;
					squadSize = new int[]{3,4,4,5,5};
					phaseDuration = new long[]{1500,1500,1500,1500};
					missionDuration = new int[]{3000,3000,3000,4500,3000};
					taskCount = new int[]{25,30,30,35,35};
					break;
					
			default: traitorCount = 0;
					squadSize = null;
					phaseDuration = null;
					missionDuration = null;
					break;
		}
	}
	
	public int getTraitorCount(){return traitorCount;}
	
	public int getSquadSize(int missionNum){return squadSize[missionNum];}
	
	public long getPhaseDuration(int phaseNum){return phaseDuration[phaseNum];}
	
	public int getMissionDuration(int missionNum){return missionDuration[missionNum];}
}
