package game;

import java.awt.Color;

public class Player {
	//Roles and states
	private boolean leader = false;
	private boolean traitor = false;
	private boolean onMission = false;
	
	private Color color = null;
	private String strColor = null;
	
	//Log in status
	private boolean registered;
	
	private int misclickNum = 0;
	
	//Player Info
	private String playerName;
	private String role;
	
	public Player(boolean loggedIn) {
		registered = loggedIn;
	}
	
	public String getPlayerName() {
		return playerName;
	}
	
	public void setPlayerName(String name) {
		playerName = name;
	}
	
	public boolean isLeader() {
		return leader;
	}
	
	public void setLeader(boolean isLeader) {
		leader = isLeader;
	}
	
	public boolean isTraitor() {
		return traitor;
	}
	
	public String getRole() {
		return role;
	}
	
	public void setTraitor(boolean isTraitor) {
		traitor = isTraitor;
		if(traitor) {
			role = "Traitors";
		} else {
			role = "Patriots";
		}
	}
	
	public boolean isOnMission() {
		return onMission;
	}
	
	public void setOnMission(boolean isOnMission) {
		onMission = isOnMission;
	}
	
	public void setColor(Color color) {
		this.color = color;
		if(color.equals(Color.RED)) strColor = "Red";
		else if(color.equals(Color.BLUE)) strColor = "Blue";
		else if(color.equals(Color.GREEN)) strColor = "Green";
		else if(color.equals(Color.YELLOW)) strColor = "Yellow";
	}
	
	public Color getColor() {
		return color;
	}
	
	public String getStrColor() {
		return strColor;
	}
	
	public void addMisclick() {
		misclickNum+=1;
	}
	
	public boolean checkMisclick() {
		if(misclickNum>=3) {
			misclickNum = 0;
			return true;
		} else {
			return false;
		}
	}

}
