package game;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class TaskManager {
	private static final String filePath = "src/game/commands";
	private static Map<Integer,String> taskMap;
	
	 {
		//Initialize tasks from file
		taskMap = new HashMap<Integer,String>();
		try {
			Scanner sc = new Scanner(new File(filePath));
			int index = 0;
			while(sc.hasNextLine()) {
				taskMap.put(index, sc.nextLine());
				index++;
			}
			sc.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public ArrayList<Task> getShuffledTaskList() {
		ArrayList<Task> tempList = new ArrayList<Task>(16);
		for (Map.Entry<Integer, String> entry : taskMap.entrySet()) {
		    Task task = new Task(entry.getKey(),entry.getValue());
		    tempList.add(task);
		}
		Collections.shuffle(tempList);
		return tempList;
	}
	
}
