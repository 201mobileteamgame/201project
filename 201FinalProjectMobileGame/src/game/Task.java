package game;

public class Task {

	private final int button;
	private final String instruction;
	
	public Task(int buttonNum, String taskStr) {
		button = buttonNum;
		instruction = taskStr;
	}
	
	public String getInstruction() {
		return instruction;
	}
	
	public int getButton() {
		return button;
	}
	
}
